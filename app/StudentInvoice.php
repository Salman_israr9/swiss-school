<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

class StudentInvoice extends Model
{
    protected $table ="sinvoice";

    protected $primarykey =['ID','StudentID'];
    public $timestamps=false;
    protected $fillable =['ID','StudentID'];
    

    public function invoice()
    {
        return $this->hasOne('App\Invoice','ID');
    }

}
