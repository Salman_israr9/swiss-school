<?php

namespace App\status;

use Illuminate\Database\Eloquent\Model;

class Status extends Model
{


    protected $primaryKey = 'StatusID';
    protected $table="status";
    protected $fillable =['StatusName'];
    public $timestamps = false;

    public function students()
    {
    	return $this->belongsToMany('App\students\Student', 'studentstatus','StudentID', 'StudentStatusID');
    }

}
