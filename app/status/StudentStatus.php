<?php

namespace App\status;

use Illuminate\Database\Eloquent\Model;

class StudentStatus extends Model
{
    protected $primaryKey = 'ID';
    protected $table="studentstatus";
    protected $fillable =['StudentID','StudentStatusID','StudentStatusDate','StudentStatusComment'];
    public $timestamps = false;
    public function status(){
        return $this->hasOne('App\status\Status','StatusID','ID');
    }
    public function student(){
       // return $this->hasOne('App\students\Student','ID','StudentID');
        return $this->hasOne('App\students\Student','ID','StudentID')->with("program")->with("currentStatus")->orderBy('ID','desc');
    }
    
  
}
