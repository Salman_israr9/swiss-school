<?php
namespace App;
use Illuminate\Database\Eloquent\Model;
class Program extends Model
{
    protected $primaryKey ='ID';
    protected $fillable =['ProgramTitle','ProgramCode','ProgramType','AcademicYear','Notes'];
    protected $table="program";
    public $timestamps = false;

    public function course(){
       return  $this->belongsToMany('App\Course','belong','ProgramID','CourseID');
    }

    public function student()
    {
        return $this->belongsToMany('App\students\Student', 'graduate', 'ProgramID', 'StudentID');
    }

    public function invoice()
    {
        return $this->belongsToMany('App\Invoice', 'cinvoice', 'CompanyID', 'ID');
    }
}
