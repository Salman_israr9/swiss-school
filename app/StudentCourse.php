<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class StudentCourse extends Model
{
    protected $table ="attend";
    protected $fillable=['FinalExameDate','Enrolled','Percentage','DescriptiveGrade','Grade','ECTSGrade','ImportFlag','ImportUser'];
}
