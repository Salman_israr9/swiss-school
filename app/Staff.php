<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Staff extends Model
{
    protected $primaryKey ='ID';
    protected $table="staff";
    public $timestamps = false;
    protected $fillable =['FirstName','LastName','Title','BirthDate','BirthPlace','BirthCountry','StreetAddress','City','Zip','Country','Phone','Mobile','Fax','Email','Email2','Email3','WebAddress','Photo','Department','Position'];
 
}
