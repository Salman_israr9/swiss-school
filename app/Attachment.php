<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Attachment extends Model
{
    protected $table="attachments";
    protected $primarykey ='ID';
    protected $fillable=[
        'Description',
        'File',
        'AttachmentDate'
    ];
    public $timestamps = false;
}
