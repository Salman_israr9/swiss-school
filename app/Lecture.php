<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Lecture extends Model
{
    protected $primaryKey ='ID';
    protected $table="lecture";
    public $timestamps = false;
    protected $fillable =['ProfessorID','CourseID','ProgramID','','AcademicYear','Term'];
}
