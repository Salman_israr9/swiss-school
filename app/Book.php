<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Book extends Model
{
    protected $table="book";
    protected $primarykey ='ID';
    protected $fillable=[
        'ID',
        'Title',
        'Author',
        'Publisher',
        'Edition',
        'ISBN',
        'Year',
        'Cover',
        'OnStock',
        'TS',
    ];
    public $timestamps = false;
    public function student()
    {
        return $this->belongsToMany('App\students\Student', 'received', 'BookID', 'StudentID');
    }

    public function course()
    {
        return $this->belongsToMany('App\Course', 'coursebook', 'BookID','CourseID');
    }

    public function bookbundle()
    {
        return $this->belongsToMany('App\BookBundle', 'bbelong', 'BookID','BookBundleID');
    }
}
