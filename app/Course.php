<?php
namespace App;
use Illuminate\Database\Eloquent\Model;
class Course extends Model
{
    protected $primaryKey ='ID';
    protected $table="course";
    protected $fillable =['CourseTitle','CourseCode','ECTSCredits','SBSCredits'];
    public $timestamps = false;

    /// get all program of this course
    public function program(){
        return $this->belongsToMany('App\Program','belong','CourseID','ProgramID');
    }

    /// get all student of this course
    public function student()
    {
        return $this->belongsToMany('App\students\Student', 'attend2', 'CourseID', 'StudentID');
    }

    public function partner()
    {
        return $this->belongsToMany('App\partner\Partner', 'partnercourse', 'CourseID','PartnerID');
    }

    public function book()
    {
        return $this->belongsToMany('App\Book', 'coursebook', 'CourseID','BookID');
    }

    public function professor()
    {
        return $this->belongsToMany('App\Professor', 'lecture', 'CourseID','ProfessorID');
    }

}
