<?php
namespace App;
use Illuminate\Database\Eloquent\Model;
class Invoice extends Model
{
    protected $table ="invoice";
    protected $primaryKey ='ID';
    protected $fillable =['InvoicePurpose','InvoiceNumber','Ammount','Currency','InvoiceDate','DueDate','PaymentDate','Paid'];
    public $timestamps = false;

    public function students()
    {
        return $this->belongsToMany('App\students\Student', 'sinvoice', 'ID', 'StudentID');
    }
    public function company()
    {
        return $this->belongsToMany('App\Company', 'cinvoice', 'ID', 'CompanyID');
    }



    public function sinvoice()
    {
        return $this->belongsTo('App\StudentInvoice', 'ID');
    }

    }