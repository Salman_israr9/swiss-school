<?php

namespace App\partner;

use Illuminate\Database\Eloquent\Model;
use App\students\Student;
class Partner extends Model
{
   protected $fillable =['PartnerCode','PartnerName','PartnerActive','PartnerSortOrder'];
   protected $table="partner";
   protected $primaryKey ='PartnerID';
   public function student()
   {
       return $this->hasMany('App\students\Student', 'PartnerID');
   }

   public function course()
   {
       return $this->belongsToMany('App\Course', 'partnercourse', 'PartnerID', 'CourseID');
   }
}
