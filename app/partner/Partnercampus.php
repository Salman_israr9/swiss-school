<?php

namespace App\partner;

use Illuminate\Database\Eloquent\Model;

class Partnercampus extends Model
{
    protected $fillable = ['PartnerID','PartnerCampusLocation','PartnerCampusActive','PartnerCampusSortOrder'];

    protected $table="partnercampus";

    public function student()
    {
        return $this->hasMany('App\students\Student', 'PartnerCampusID');
    }

}
