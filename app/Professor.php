<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Professor extends Model
{
    protected $primaryKey ='ID';
    protected $table="professor";

    protected $fillable = ['Title', 'FirstName', 'LastName', 'SendEmail', 'BirthDate', 'BirthPlace', 'BirthCountry', 'StreetAddress', 'City', 'Zip', 'Country', 'Phone', 'Mobile', 'Email', 'Email2', 'Email3', 'Notes','remarks'];
    public $timestamps = false;


    public function student()
    {
        return $this->belongsToMany('App\students\Student', 'business', 'ProfessorID', 'StudentID');
    }
    public function course()
    {
        return $this->belongsToMany('App\Course', 'lecture','ProfessorID', 'CourseID');
    }

    public function program()
    {
        return $this->belongsToMany('App\Program', 'lecture','ProfessorID', 'ProgramID');
    }
}
