<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class GraduateReq extends Model
{
    protected $primaryKey ='ID';
    protected $table="graduatereq";
    protected $fillable = ['RequirementID','StudentID','RequirementValue','RequirementDate','RequirementComment'];
    public $timestamps = false;

}
