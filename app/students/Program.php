<?php

namespace App\students;

use Illuminate\Database\Eloquent\Model;

class Program extends Model
{
    protected $fillable = ['studentid', 'program', 'secondary_major', 'examiner_1', 'examiner_2', ' examiner_3', 'thesis_grade', 'thesis_mentor', 'thesis_percentage', 'thesis_subject', 'books_included', 'withdrawn',  'graduated'];
    
       
}
