<?php

namespace App\students;

use Illuminate\Database\Eloquent\Model;

class Business extends Model
{
    protected $primarykey='ID';
    protected $table="business";
    public $timestamps = false;
    protected $fillable = ['StudentID','CompanyName', 'Department', 'Position', 'StreetAddress', 'City', ' Zip', 'Country', 'Phone', 'Mobile', 'Email', 'Fax', 'Notes', 'WebAddress', 'ContactPersonTitle', 'ContactPersonFirstName', 'ContactPersonLastName', 'ContactPersonDepartment','ProfessorID'];
}
