<?php

namespace App\students;

use Illuminate\Database\Eloquent\Model;

class Remarks extends Model
{
    protected $fillable = ['remarks','studentid'];

}
