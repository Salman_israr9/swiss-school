<?php

namespace App\students;

use Illuminate\Database\Eloquent\Model;

class GradutionReq extends Model
{
    protected $table="graduaterequirement";
    public $timestamps = false;


    protected $fillable = ['StudentID', 'BBAExitSurvey','BBAExitSurveyCBE', 'MSCInternReport', 'MASExitSurvey', 'MASExitExamSBS', ' MASExitExamExternal', 'DBAExitSurvey','DBAExitExamSBS','DBAExitExamExternal','DBAEthicsApprovalDoc','DBAEApprovalDate','DBAStatsExamDate','DBAStatsExamResult','DBACompsDate','DBACompsResult','DBAMAIssued','DBAMADate','DBADissertation','DBADODDate','DBADegreeIssued','DBADegreeIssueDate','PassingGPA','FinancialObligations'];

}
