<?php

namespace App\students;

use Illuminate\Database\Eloquent\Model;

class Grade extends Model
{
    protected $primaryKey ='ID';
    protected $table="attend";
    public $timestamps = false;
    protected $fillable =
    ['StudentID',
    'FinalExameDate',
    'Enrolled',
    'Percentage',
    'DescriptiveGrade',
    'Grade',
    'ECTSGrade',
    'ImportFlag',
    'ImportUser'
    ];
    
}
