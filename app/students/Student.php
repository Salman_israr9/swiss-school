<?php

namespace App\students;
use App\partner\Partner;
use Illuminate\Database\Eloquent\Model;

class Student extends Model
{
    protected $primaryKey ='ID';
    public $timestamps = false;
    protected $table="student";

    protected $fillable = ['StudentNumber', 'Title', 'FirstName', 'LastName', 'TranscriptName', 'BirthDate', 'BirthPlace', 'BirthCountry', 'StreetAddress', 'City', 'Zip', 'Country', 'Phone', 'Mobile', 'Email', 'Email2', 'Email3', 'Notes','PartnerID','PartnerCampusID'];
    
    public function partner()
    {
        return $this->belongsTo('App\partner\Partner', 'PartnerID');
    }

    public function partnercampus()
    {
        return $this->belongsTo('App\partner\Partnercampus', 'PartnerCampusID');
    }

    public function statuses()
    {
    	return $this->hasMany('App\status\StudentStatus','StudentID')->orderBy('StudentStatusDate','desc');
    }
    public function currentStatus()
    {
    	return $this->hasMany('App\status\StudentStatus','StudentID')->with('status')->orderBy('StudentStatusDate','desc');
    
    }

    public function s_status()
    {
        
    }


    public function invoice()
    {
        return $this->belongsToMany('App\Invoice', 'sinvoice', 'StudentID', 'ID');
    }

    public function course()
    {
        return $this->belongsToMany('App\Course', 'attend2', 'StudentID', 'CourseID');
    }

    public function bookbundle()
    {
        return $this->belongsToMany('App\BookBundle', 'bbbelong', 'StudentID', 'BookBundleID');
    }

    public function book()
    {
        return $this->belongsToMany('App\Book', 'received', 'StudentID', 'BookID');
    }

    public function program()
    {
        return $this->belongsToMany('App\Program', 'graduate', 'StudentID', 'ProgramID');
    }


    public function professor()
    {
        return $this->belongsToMany('App\Professor', 'business', 'StudentID', 'ProfessorID');
    }

    public function graduationreqitem()
    {
        return $this->belongsToMany('App\GraduationReqItem', 'graduatereq', 'StudentID', 'RequirementID');
    }
 




}
