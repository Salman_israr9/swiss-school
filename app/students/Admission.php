<?php

namespace App\students;

use Illuminate\Database\Eloquent\Model;

class Admission extends Model
{
    protected $primaryKey ='ID';
    protected $table="admission";
    public $timestamps = false;
    protected $fillable = 
    ['EducationalBackground',
    'SBSTest',
    'GMATTest',
    'GMATResult',
    'SATTest',
    'SATResult',
    'TOEFLTest',
    'TOEFLResult',
    'IELTSTest',
    'IELTSResult',
    'ExamAdmBBA',
    'ExamAdmResultBBA',
    'ExamEntBBA',
    'ExamEntResultBBA',
    'ExamOthBBA',
    'ExamOthResultBBA',
    'ExamAdmMAS',
    'ExamAdmResultMAS',
    'ExamEntMAS',
    'ExamEntResultMAS',
    'ExamOthMAS',
    'ExamOthResultMAS',
    'ExamEntDBA',
    'ExamEntResultDBA',
    'ExamOthDBA',
    'ExamOthResultDBA',
    'StudentID'
    ];

}
