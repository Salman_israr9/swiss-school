<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class GraduateReqItem extends Model
{
    public function student()
    {
        return $this->belongsToMany('App\students/Student', 'graduatereq', 'RequirementID', 'StudentID');
    }
}
