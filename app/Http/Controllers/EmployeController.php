<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Professor;
use App\students\Business;
use App\Lecture;
use App\Course;
use App\Program;
use App\Staff;

class EmployeController extends Controller
{
    public function index()
    {
        $professor = Professor::paginate(10);
        $response = [
            'pagination' => [
                'total' => $professor->total(),
                'per_page' => $professor->perPage(),
                'current_page' => $professor->currentPage(),
                'last_page' => $professor->lastPage(),
                'from' => $professor->firstItem(),
            'to' => $professor->lastItem()
            ],
            'data' => $professor
        ];
        return response()->json($response);
    }
    public function saveProfessor(Request  $request)
    {
        $image_name="";
        if($request->image != '')
        {
            $image_name=$request->image->getClientOriginalName();
            $request->image->move(public_path('/professor_images'),$image_name);
        }

        $p = json_decode($request->professor);
        $pro= new Professor();
        $pro->FirstName = $p->FirstName;	
        $pro->LastName = $p->LastName;	
        $pro->Title = $p->Title;	

        // $pro->BirthDate = $p->BirthDate;	
        $pro->BirthDate = $p->BirthDate;	

        $pro->BirthPlace = $p->BirthPlace;	
        $pro->BirthCountry = $p->BirthCountry;	
        $pro->StreetAddress = $p->StreetAddress;	
        $pro->City = $p->City;	
        $pro->Zip = $p->Zip;	
        $pro->Country = $p->Country;	
        $pro->Phone = $p->Phone;	
        $pro->Mobile = $p->Mobile;	
        $pro->Fax = $p->Fax;	
        $pro->Email = $p->Email;	
        $pro->Email2 = $p->Email2;	
        $pro->Email3 = $p->Email3;	
        $pro->WebAddress = $p->WebAddress;	
        $pro->Photo = $p->Photo;	
        $pro->Remarks = $p->Remarks;	
        $pro->Notes = $p->Notes;	
        $pro->Department = $p->Department;	
        $pro->Position = $p->Position;	
        $pro->MailAddress = $p->MailAddress;	
        $pro->SendEmail = $p->SendEmail;
        $pro->save();

        // save bussiness
        $pBussiness = json_decode($request->bussiness);
        $business = new Business();
        $business->ProfessorID=$pro->ID;
        $business->ContactPersonTitle=$pBussiness->ContactPersonTitle;
        $business->ContactPersonLastName=$pBussiness->ContactPersonLastName;
        $business->CompanyName=$pBussiness->CompanyName;
        $business->Department=$pBussiness->Department;
        $business->Position=$pBussiness->Position;
        $business->StreetAddress=$pBussiness->StreetAddress;
        $business->Zip=$pBussiness->Zip;
        $business->Phone=$pBussiness->Phone;
        $business->Email=$pBussiness->Email;
        $business->Fax=$pBussiness->Fax;
        $business->Notes=$pBussiness->Notes;
        $business->Mobile=$pBussiness->Mobile;
        $business->Country=$pBussiness->Country;
        $business->City=$pBussiness->City;
        $business->WebAddress=$pBussiness->WebAddress;
        $business->ContactPersonFirstName=$pBussiness->ContactPersonFirstName;
        $business->ContactPersonDepartment=$pBussiness->ContactPersonDepartment;
        $business->save();

        if(isset($request->lectures)){
            foreach( $request->lectures as $lect){
                $lecture = json_decode($lect);
                $lec = new Lecture();
                $lec->ProfessorID = $pro->ID;
                $lec->CourseID = $lecture->cId;
                $lec->ProgramID = $lecture->pId;
                $lec->AcademicYear = explode("-", $lecture->aTitle)[0];
                $lec->Term = $lecture->term;
                $lec->save();
            }
        }

        return response()->json('success');
    }
    public function store(Request $request)
    {
        //
    }
    public function editProfessor($id)
    {
        $professor =Professor::where('ID',$id)->first();
        $business =Business::where('ProfessorID',$id)->first();
        $lectures =Lecture::where('ProfessorID',$id)->get();

        $all_lectures =[];
        foreach ($lectures as $lec)
        {

            $all_lectures[] = ['ID'=>$lec->ID,'cId'=>$lec->CourseID,'cTitle'=>Course::where('ID',$lec->CourseID)->first()->CourseTitle,'pId'=>$lec->ProgramID,'pTitle'=>Program::where('ID',$lec->ProgramID)->first()->ProgramTitle,'aId'=>null,'aTitle'=>$lec->AcademicYear,'term'=>$lec->Term];
            
        }
        return response()->json(['professor'=>$professor,'bussiness'=>$business,'lectures'=>$all_lectures]);
    }
    public function edit($id)
    {
        //
    }
    public function updateProfessor(Request $request)
    {
        
        $image_name="";
        if($request->image != '')
        {
            $image_name=$request->image->getClientOriginalName();
            $request->image->move(public_path('/professor_images'),$image_name);
        }

        $p = json_decode($request->professor);
        $pro=Professor::where("ID",$p->ID)->first();
        $pro->FirstName = $p->FirstName;	
        $pro->LastName = $p->LastName;	
        $pro->Title = $p->Title;	
        // $pro->BirthDate = $p->BirthDate;	
        $pro->BirthPlace = $p->BirthPlace;	
        $pro->BirthCountry = $p->BirthCountry;	
        $pro->StreetAddress = $p->StreetAddress;	
        $pro->City = $p->City;	
        $pro->Zip = $p->Zip;	
        $pro->Country = $p->Country;	
        $pro->Phone = $p->Phone;	
        $pro->Mobile = $p->Mobile;	
        $pro->Fax = $p->Fax;	
        $pro->Email = $p->Email;	
        $pro->Email2 = $p->Email2;	
        $pro->Email3 = $p->Email3;	
        $pro->WebAddress = $p->WebAddress;	
        $pro->Photo = $p->Photo;	
        $pro->Remarks = $p->Remarks;	
        $pro->Notes = $p->Notes;	
        $pro->Department = $p->Department;	
        $pro->Position = $p->Position;	
        $pro->MailAddress = $p->MailAddress;	
        $pro->SendEmail = $p->SendEmail;
        $pro->save();

        // save bussiness
        $pBussiness = json_decode($request->bussiness);
        $business = Business::where("ID",$pBussiness->ID)->first();
        $business->ProfessorID=$pro->ID;
        $business->ContactPersonTitle=$pBussiness->ContactPersonTitle;
        $business->ContactPersonLastName=$pBussiness->ContactPersonLastName;
        $business->CompanyName=$pBussiness->CompanyName;
        $business->Department=$pBussiness->Department;
        $business->Position=$pBussiness->Position;
        $business->StreetAddress=$pBussiness->StreetAddress;
        $business->Zip=$pBussiness->Zip;
        $business->Phone=$pBussiness->Phone;
        $business->Email=$pBussiness->Email;
        $business->Fax=$pBussiness->Fax;
        $business->Notes=$pBussiness->Notes;
        $business->Mobile=$pBussiness->Mobile;
        $business->Country=$pBussiness->Country;
        $business->City=$pBussiness->City;
        $business->WebAddress=$pBussiness->WebAddress;
        $business->ContactPersonFirstName=$pBussiness->ContactPersonFirstName;
        $business->ContactPersonDepartment=$pBussiness->ContactPersonDepartment;
        $business->save();

        if(isset($request->lectures)){
             Lecture::where("ProfessorID",$pro->ID)->delete();
            foreach( $request->lectures as $lect){
                $lecture = json_decode($lect);
                //  if($lecture->ID != -1)
                // $lec =Lecture::where("ID",$lecture->ID)->first();
                // else
                $lec = new Lecture();
                $lec->ProfessorID = $pro->ID;
                $lec->CourseID = $lecture->cId;
                $lec->ProgramID = $lecture->pId;
                $lec->AcademicYear = explode("-", $lecture->aTitle)[0];
                $lec->Term = $lecture->term;
                $lec->save();
            }
        }

        return response()->json('success');
    }
    public function destroy($id)
    {
        $pro=Professor::where("ID",$id)->first();
        $pro->delete();
        return response()->json(['Professor'=>$pro,"Professor deleted..."] );
    }

    public function search(Request $request)
    {
        $student = Professor::Where('Position','LIKE','%'.$request->search.'%')->paginate(30);
        $response = [
            'pagination' => [
                'total' => $student->total(),
                'per_page' => $student->perPage(),
                'current_page' => $student->currentPage(),
                'last_page' => $student->lastPage(),
                'from' => $student->firstItem(),
                'to' => $student->lastItem()
            ],
            'data' => $student
        ];
        return response()->json($response);
    }
    public function getpro()
    {
       $professor = Professor::all();
       return response()->json($professor);
    }
    public function StaffCreate(Request $request)
    {
        $staff = new Staff();

        $staff->FirstName=$request->FirstName;
        $staff->LastName=$request->LastName;
        $staff->Title=$request->Title;
        $staff->BirthDate=$request->BirthDate;
        $staff->BirthPlace=$request->BirthPlace;
        $staff->BirthCountry=$request->BirthCountry;
        $staff->StreetAddress=$request->StreetAddress;
        $staff->City=$request->City;
        $staff->Zip=$request->Zip;
        $staff->Country=$request->Country;
        $staff->Phone=$request->Phone;
        $staff->Mobile=$request->Mobile;
        $staff->Fax=$request->Fax;
        $staff->Email=$request->Email;
        $staff->Email2=$request->Email2;
        $staff->Email3=$request->Email3;
        $staff->WebAddress=$request->WebAddress;
        $staff->Department=$request->Department;
        $staff->Photo=$request->Photo;
        $staff->Position=$request->Position;
        $staff->save();
        return response()->json($staff);

    }
    public function stafflist()
    {
        $staff = Staff::paginate(10);
        $response = [
            'pagination' => [
                'total' => $staff->total(),
                'per_page' => $staff->perPage(),
                'current_page' => $staff->currentPage(),
                'last_page' => $staff->lastPage(),
                'from' => $staff->firstItem(),
            'to' => $staff->lastItem()
            ],
            'data' => $staff
        ];
        return response()->json($response);
    }
    public function deletestaff($id)
    {
        $staff= Staff::where('ID',$id)->first();
        $staff->delete();
        return response()->json("Deleted secussfully");
    }

    public function searchstaff(Request $request)
    {
        $staff = Staff::Where('Position','LIKE','%'.$request->search.'%')->paginate(30);
        $response = [
            'pagination' => [
                'total' => $staff->total(),
                'per_page' => $staff->perPage(),
                'current_page' => $staff->currentPage(),
                'last_page' => $staff->lastPage(),
                'from' => $staff->firstItem(),
                'to' => $staff->lastItem()
            ],
            'data' => $staff
        ];
        return response()->json($response);
    }
    public function getStaffByID($id)
    {
      $staff =Staff::where('ID',$id)->first();
      return response()->json($staff);
    }
    public function updateStaff(Request $request,$id)
    {
        $staff=Staff::where('ID',$id)->first();

        $staff->FirstName=$request->FirstName;
        $staff->LastName=$request->LastName;
        $staff->Title=$request->Title;
        $staff->BirthDate=$request->BirthDate;
        $staff->BirthPlace=$request->BirthPlace;
        $staff->BirthCountry=$request->BirthCountry;
        $staff->StreetAddress=$request->StreetAddress;
        $staff->City=$request->City;
        $staff->Zip=$request->Zip;
        $staff->Country=$request->Country;
        $staff->Phone=$request->Phone;
        $staff->Mobile=$request->Mobile;
        $staff->Fax=$request->Fax;
        $staff->Email=$request->Email;
        $staff->Email2=$request->Email2;
        $staff->Email3=$request->Email3;
        $staff->WebAddress=$request->WebAddress;
        $staff->Department=$request->Department;
        $staff->Photo=$request->Photo;
        $staff->Position=$request->Position;
        $staff->save();
        return response()->json($staff);
    }
}
