<?php

namespace App\Http\Controllers\students;
use App\students\Remarks;
use App\students\Program;
use App\students\Grade;
use App\students\Business;
use App\students\Admission;
use App\status\StudentStatus;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\students\GradutionReq;
use App\status\Status;
use App\students\Student;
use App\StudentInvoice;
use App\Invoice;
use App\Course;
use App\Attachment;
use App\StudentProgram;
use Illuminate\Support\Facades\DB;
//use Zend\Diactoros\Request;
class StudentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

     public function getStudentByName(Request $request)
     {

         return Student::where('FirstName','LIKE','%'.$request->name.'%')->get();
     }

     public function getInvoiceNo()
     {
        return  response()->json(Invoice::all()->last()->InvoiceNumber + 1);
     }

     public function gsi()
     {
        // return  DB::table('sinvoice')
        // ->join('invoice', 'sinvoice.ID', '=', 'invoice.ID')
        // ->select('sinvoice.StudentID', 'invoice.*')
        // ->get();

        return  DB::table('cinvoice')
        ->join('invoice', 'cinvoice.ID', '=', 'invoice.ID')->where('invoice.Paid','=',-1)
        ->select('cinvoice.CompanyID', 'invoice.*')
        ->paginate();

        return  DB::table('cinvoice')
        ->join('invoice', 'cinvoice.ID', '=', 'invoice.ID')
        ->select('cinvoice.CompanyID', 'invoice.*')
        ->get();

         return StudentInvoice::with('invoice')->get();
         

     }
    public function index()
    {

        $students = Student::orderBy('ID','desc')->with('partner')->with("program")->with("currentStatus")->paginate(10);

        $students = Student::orderBy('ID','desc')->with('partner')->with("program")->with("currentStatus")->paginate(12);
        // $students=  DB::table('student')
        // ->join('graduate', 'student.ID', '=', 'graduate.StudentID')
        // ->join('program', 'Program.ID', '=', 'graduate.ProgramID')
        // ->join('status', 'studentstatus.StudentStatusID', '=', 'status.StatusID')->where("StudentStatusID",$id)
        // ->select( 'student.ID as stdID','student.*','status.*')->paginate(10);
    //    return  $students = StudentStatus::where('StudentStatusID',1)->with('student')->paginate(2);

        $response = [
            'pagination' => [
                'total' => $students->total(),
                'per_page' => $students->perPage(),
                'current_page' => $students->currentPage(),
                'last_page' => $students->lastPage(),
                'from' => $students->firstItem(),
            'to' => $students->lastItem()
            ],
            'data' => $students
        ];
        return response()->json($response);
    }

    public function seacrhByStatus($id){
        
        return  DB::table('studentstatus')
        ->join('student', 'studentstatus.StudentID', '=', 'student.ID')
        ->join('graduate', 'student.ID', '=', 'graduate.StudentID')
        ->join('program', 'Program.ID', '=', 'graduate.ProgramID')
        ->join('status', 'studentstatus.StudentStatusID', '=', 'status.StatusID')->where("StudentStatusID",$id)
        ->select( 'student.ID as stdID','student.*','status.*')->paginate(10);

        // return $deliveries = Student::with(array('currentStatus' => function($query)
        //         {
        //             $query->where('studentstatus.StudentStatus', $id);
        //             $query->orderBy('studentstatus.id', 'DESC');
        //         }))->get();

       
        // ->paginate();
        
    //return $students = StudentStatus::where('StudentStatusID',$id)->with('student')->with('status')->get();
    //  dd($students );

    // $students =null;
    // if($request->country_name != '0')
    // $students = Student::orderBy('ID','desc')->with('partner')->with("program")->with("currentStatus")->paginate(10);
     
    // if($request->country_name != '0' && $request->partner_id !=0)
    // $students = Student::orderBy('ID','desc')->where("Country",$request->country_name)->where("PartnerID",$request->partner_id)->with('partner')->with("program")->with("currentStatus")->paginate(10);
   
       $response = [
           'pagination' => [
               'total' => $students->total(),
               'per_page' => $students->perPage(),
               'current_page' => $students->currentPage(),
               'last_page' => $students->lastPage(),
               'from' => $students->firstItem(),
           'to' => $students->lastItem()
           ],
           'data' => $students
       ];
       return response()->json($response);

    }

    public function searchByFilterList(Request $request)
    {
        return  DB::table('studentstatus')
        ->join('student', 'studentstatus.StudentID', '=', 'student.ID')
        ->join('graduate', 'student.ID', '=', 'graduate.StudentID')
        ->join('program', 'Program.ID', '=', 'graduate.ProgramID')
        ->join('status', 'studentstatus.StudentStatusID', '=', 'status.StatusID')->where("StudentStatusID",$request->statsu_id)
        ->select( 'student.ID as stdID','student.*','status.*')->paginate(10);
        // return Student::orderBy('ID','desc')->where("Country",$request->country_name)->with('partner')->with("program")->with("currentStatus")->paginate(10);

        // $students = StudentStatus::where('StudentStatusID',$request->status_id)->with('student')->paginate(10);
       
        //  = DB::table('studentstatus')
        // ->join('student', 'studentstatus.StudentID', '=', 'student.ID')
        // ->join('graduate', 'student.ID', '=', 'graduate.StudentID')
        // ->join('program', 'Program.ID', '=', 'graduate.ProgramID')
        // ->join('status', 'studentstatus.StudentStatusID', '=', 'status.StatusID')->where("StudentStatusID",$request->status_id)
        // ->select('status.*', 'student.*')
        // ->paginate();

    //     $students =[];
    //     if($request->country_name != '0')
    //    {
    //     $students = Student::orderBy('ID','desc')->where("Country",$request->country_name)->with('partner')->with("program")->with("currentStatus")->paginate(10);
       
    //    }
    //    else 
    //     if($request->country_name != '0' && $request->partner_id !=0)
    //     {

    //         $students = Student::orderBy('ID','desc')->where("Country",$request->country_name)->where("PartnerID",$request->partner_id)->with('partner')->with("program")->with("currentStatus")->paginate(10);
    //     }
        
        $response = [
            'pagination' => [
                'total' => $students->total(),
                'per_page' => $students->perPage(),
                'current_page' => $students->currentPage(),
                'last_page' => $students->lastPage(),
                'from' => $students->firstItem(),
                 'to' => $students->lastItem()
            ],
            'data' => $students
        ];
        return response()->json($response);
    }

    public function store(Request $request)
    {
        $std = new Student();
        $image_name='';
        if($request->image!='')
        {
            $image_name=$request->student_image->getClientOriginalName();
            $request->student_image->move(public_path('/student_images'),$image_name);
        }
         $student= json_decode($request->student);
        $std->StudentNumber=$student->StudentNumber;
        $std->FirstName=$student->FirstName;
        $std->LastName=$student->LastName;
        $std->TranscriptName=$student->TranscriptName;
        $std->Title=$student->Title;
        $std->BirthDate=$student->BirthDate;
        $std->BirthPlace=$student->BirthPlace;
        $std->BirthCountry=$student->BirthCountry;
        $std->StreetAddress=$student->StreetAddress;
        $std->City=$student->City;
        $std->Zip=$student->Zip;
        $std->Country=$student->Country;
        $std->Phone=$student->Phone;
        $std->Mobile=$student->Mobile;
        $std->Fax=$student->Fax;
        $std->Email=$student->Email;
        $std->Email2=$student->Email2;
        $std->Email3=$student->Email3;
        $std->WebAddress=$student->WebAddress;
        $std->Photo=$image_name;
        $std->Remarks=$student->Remarks;
        $std->Notes=$student->Notes;
        $std->MailAddress=$student->MailAddress;
        $std->SISEnabled=$student->SISEnabled;
        $std->GradeNotification=$student->GradeNotification;
        if($student->PartnerID != 0)
        {
         $std->PartnerID=$student->PartnerID;
        }
        if($student->PartnerCampusID != 0)
        $std->PartnerCampusID=$student->PartnerCampusID;


        // // $std->Student=$student->Student;
        $std->ImportUser=$student->ImportUser;
         $std->save();

        //graduation requirment

        $stdGraduateReq= json_decode($request->stdGraduateReq);
        $graduation = new GradutionReq();
        $graduation->DBAExitSurvey=$stdGraduateReq->DBAExitSurvey;
        $graduation->DBAExitExamSBS=$stdGraduateReq->DBAExitExamSBS;
        $graduation->DBAExitExamExternal=$stdGraduateReq->DBAExitExamExternal;
        $graduation->MASExitExamSBS=$stdGraduateReq->MASExitExamSBS;
        $graduation->MASExitExamExternal=$stdGraduateReq->MASExitExamExternal;
        $graduation->BBAExitSurveyCBE=$stdGraduateReq->BBAExitSurveyCBE;
        $graduation->MASExitSurvey=$stdGraduateReq->MASExitSurvey;
        $graduation->MSCInternReport=$stdGraduateReq->MSCInternReport;
        $graduation->BBAExitSurvey=$stdGraduateReq->BBAExitSurvey;
        $graduation->BBAExitSurveyCBE=$stdGraduateReq->BBAExitSurveyCBE;
        $graduation->PassingGPA=$stdGraduateReq->PassingGPA;
        $graduation->StudentID=$std->ID;
        $graduation->save();



        // save student program
        $stdProgram = json_decode($request->stdProgram);
        $s_p  = new StudentProgram();
        $s_p->StudentID=$std->ID;
        $s_p->ProgramID=$stdProgram->ProgramID;
        $s_p->Program2ID=$stdProgram->Program2ID;
        $s_p->BooksIncluded=$stdProgram->BooksIncluded;
        $s_p->Graduate=$stdProgram->Graduate;
        $s_p->Withdraw=$stdProgram->Withdraw;
        $s_p->GraduationDate=$stdProgram->GraduationDate;
        $s_p->ThesisPercentage=$stdProgram->ThesisPercentage;
        $s_p->ThesisDescriptiveGrade=$stdProgram->ThesisDescriptiveGrade;
        $s_p->ThesisGrade=$stdProgram->ThesisGrade;
        $s_p->ThesisECTSGrade=$stdProgram->ThesisECTSGrade;
        $s_p->ThesisSubject=$stdProgram->ThesisSubject;
        $s_p->ThesisMentor=$stdProgram->ThesisMentor;
        $s_p->Examiner1=$stdProgram->Examiner1;
        $s_p->Examiner2=$stdProgram->Examiner2;
        $s_p->Examiner3=$stdProgram->Examiner3;
        $s_p->save();

          //status
        if(isset($request->status))
         {
          foreach($request->status as $stts)
          {
            $sts = json_decode($stts);
            $status = new StudentStatus();
            $status->StudentID=$std->ID;
            $status->StudentStatusID= $sts->id;
            $status->StudentStatusDate=$sts->date;
            $status->StudentStatusComment=$sts->note;
            $status->save();
          }
        }
        //courses in grade
        if(isset($request->course))
         {
          foreach($request->course as $c)
          {
              $grade = new Grade();
              $crs = json_decode($c);
            $grade->StudentID= $std->ID;
            $grade->CourseID= $crs->ID;
            $grade->FinalExameDate=$crs->FinalExame;
            $grade->Enrolled= $crs->Enrolled;
            $grade->Percentage= $crs->Percentage;
            $grade->DescriptiveGrade= $crs->DescGrade;
            $grade->Grade=  $crs->Grade;
            $grade->ECTSGrade= $crs->ECTSGrade;
            $grade->save();
          }
        }
        //business
        $stdBussiness = json_decode($request->stdBussiness);
        $business = new Business();
        $business->StudentID=$std->ID;
        $business->ContactPersonTitle=$stdBussiness->ContactPersonTitle;
        $business->ContactPersonLastName=$stdBussiness->ContactPersonLastName;
        $business->CompanyName=$stdBussiness->CompanyName;
        $business->Department=$stdBussiness->Department;
        $business->Position=$stdBussiness->Position;
        $business->StreetAddress=$stdBussiness->StreetAddress;
        $business->Zip=$stdBussiness->Zip;
        $business->Phone=$stdBussiness->Phone;
        $business->Email=$stdBussiness->Email;
        $business->Fax=$stdBussiness->Fax;
        $business->Notes=$stdBussiness->Notes;
        $business->Mobile=$stdBussiness->Mobile;
        $business->Country=$stdBussiness->Country;
        $business->City=$stdBussiness->City;
        $business->WebAddress=$stdBussiness->WebAddress;
        $business->ContactPersonFirstName=$stdBussiness->ContactPersonFirstName;
        $business->ContactPersonDepartment=$stdBussiness->ContactPersonDepartment;
        $business->save();

         // attachments part
        if(isset($request->std_attached_files))
        {
         foreach($request->std_attached_files as $at)
         {
            $attach = json_decode($at);
            $attachment = new Attachment();
            $attachment->File =$attach->id.'/'.$attach->name;
            $attachment->StudentID = $std->ID;
            $attachment->Description = $attach->des;
            $attachment->save();
         }
       }



        /// addmission data

        $stdAddmission= json_decode($request->stdAdmission);
        $stdAdm = new Admission();
        $stdAdm->EducationalBackground=$stdAddmission->EducationalBackground;
        $stdAdm->SBSTest=$stdAddmission->SBSTest;
        $stdAdm->GMATTest=$stdAddmission->GMATTest;
        $stdAdm->GMATResult=$stdAddmission->GMATResult;
        $stdAdm->SATTest=$stdAddmission->SATTest;
        $stdAdm->SATResult=$stdAddmission->SATResult;
        $stdAdm->TOEFLTest=$stdAddmission->TOEFLTest;
        $stdAdm->TOEFLResult=$stdAddmission->TOEFLResult;
        $stdAdm->IELTSTest=$stdAddmission->IELTSTest;
        $stdAdm->IELTSResult=$stdAddmission->IELTSResult;
        $stdAdm->ExamAdmBBA=$stdAddmission->ExamAdmBBA;
        $stdAdm->ExamAdmResultBBA=$stdAddmission->ExamAdmResultBBA;
        $stdAdm->ExamEntBBA=$stdAddmission->ExamEntBBA;
        $stdAdm->ExamEntResultBBA=$stdAddmission->ExamEntResultBBA;
        $stdAdm->ExamOthBBA=$stdAddmission->ExamOthBBA;
        $stdAdm->ExamOthResultBBA=$stdAddmission->ExamOthResultBBA;
        $stdAdm->ExamAdmMAS=$stdAddmission->ExamAdmMAS;
        $stdAdm->ExamAdmResultMAS=$stdAddmission->ExamAdmResultMAS;
        $stdAdm->ExamEntMAS=$stdAddmission->ExamEntMAS;
        $stdAdm->ExamEntResultMAS=$stdAddmission->ExamEntResultMAS;
        $stdAdm->ExamOthMAS=$stdAddmission->ExamOthMAS;
        $stdAdm->ExamOthResultMAS=$stdAddmission->ExamOthResultMAS;
        $stdAdm->ExamEntDBA=$stdAddmission->ExamEntDBA;
        $stdAdm->ExamEntResultDBA=$stdAddmission->ExamEntResultDBA;
        $stdAdm->ExamOthDBA=$stdAddmission->ExamOthDBA;
        $stdAdm->ExamOthResultDBA=$stdAddmission->ExamOthResultDBA;
        $stdAdm->StudentID=$std->ID;
        $stdAdm->save();
        return response()->json([
        'Admission'=>$stdAdm,
        'Bussiness'=>$business,
        'Student'=>$std,
        'attachment'=>$attachment,
        'grade'=>$grade,
        'status'=>$status,
        'graduation'=>$graduation,
        'program'=>$s_p,
        'message'=>"Student Created successfully"
        ]);
    }

    /// stdUpdate

    public function updateStudent(Request $request,$sId)
    {
        $std = Student::where("ID",$sId)->first();
        $image_name='';
        if($request->image!='')
        {
            $image_name=$request->student_image->getClientOriginalName();
            $request->student_image->move(public_path('/student_images'),$image_name);
        }

        $student= json_decode($request->student);
        $std->StudentNumber=$student->StudentNumber;
        $std->FirstName=$student->FirstName;
        $std->LastName=$student->LastName;
        $std->TranscriptName=$student->TranscriptName;
        $std->Title=$student->Title;
        $std->BirthDate=$student->BirthDate;
        $std->BirthPlace=$student->BirthPlace;
        $std->BirthCountry=$student->BirthCountry;
        $std->StreetAddress=$student->StreetAddress;
        $std->City=$student->City;
        $std->Zip=$student->Zip;
        $std->Country=$student->Country;
        $std->Phone=$student->Phone;
        $std->Mobile=$student->Mobile;
        $std->Fax=$student->Fax;
        $std->Email=$student->Email;
        $std->Email2=$student->Email2;
        $std->Email3=$student->Email3;
        $std->WebAddress=$student->WebAddress;
        $std->Photo=$image_name;
        $std->Remarks=$student->Remarks;
        $std->Notes=$student->Notes;
        $std->MailAddress=$student->MailAddress;
        $std->SISEnabled=$student->SISEnabled;
        $std->GradeNotification=$student->GradeNotification;
        if($student->PartnerID != 0)
        {
         $std->PartnerID=$student->PartnerID;
        }
        if($student->PartnerCampusID != 0)
        $std->PartnerCampusID=$student->PartnerCampusID;


        // // $std->Student=$student->Student;
        $std->ImportUser=$student->ImportUser;
         $std->save();

        //graduation requirment
        $stdGraduateReq= json_decode($request->stdGraduateReq);
        $graduation =GradutionReq::where("StudentID",$sId)->first();
        $graduation->DBAExitSurvey=$stdGraduateReq->DBAExitSurvey;
        $graduation->DBAExitExamSBS=$stdGraduateReq->DBAExitExamSBS;
        $graduation->DBAExitExamExternal=$stdGraduateReq->DBAExitExamExternal;
        $graduation->MASExitExamSBS=$stdGraduateReq->MASExitExamSBS;
        $graduation->MASExitExamExternal=$stdGraduateReq->MASExitExamExternal;
        $graduation->BBAExitSurveyCBE=$stdGraduateReq->BBAExitSurveyCBE;
        $graduation->MASExitSurvey=$stdGraduateReq->MASExitSurvey;
        $graduation->MSCInternReport=$stdGraduateReq->MSCInternReport;
        $graduation->BBAExitSurvey=$stdGraduateReq->BBAExitSurvey;
        $graduation->BBAExitSurveyCBE=$stdGraduateReq->BBAExitSurveyCBE;
        $graduation->PassingGPA=$stdGraduateReq->PassingGPA;
        $graduation->StudentID=$std->ID;
        $graduation->save();



        // save student program
        $stdProgram = json_decode($request->stdProgram);
        $s_p =StudentProgram::where("StudentID",$sId)->first();
        $s_p->StudentID=$std->ID;
        $s_p->ProgramID=$stdProgram->ProgramID;
        $s_p->Program2ID=$stdProgram->Program2ID;
        $s_p->BooksIncluded=$stdProgram->BooksIncluded;
        $s_p->Graduate=$stdProgram->Graduate;
        $s_p->Withdraw=$stdProgram->Withdraw;
        $s_p->GraduationDate=$stdProgram->GraduationDate;
        $s_p->ThesisPercentage=$stdProgram->ThesisPercentage;
        $s_p->ThesisDescriptiveGrade=$stdProgram->ThesisDescriptiveGrade;
        $s_p->ThesisGrade=$stdProgram->ThesisGrade;
        $s_p->ThesisECTSGrade=$stdProgram->ThesisECTSGrade;
        $s_p->ThesisSubject=$stdProgram->ThesisSubject;
        $s_p->ThesisMentor=$stdProgram->ThesisMentor;
        $s_p->Examiner1=$stdProgram->Examiner1;
        $s_p->Examiner2=$stdProgram->Examiner2;
        $s_p->Examiner3=$stdProgram->Examiner3;
        $s_p->save();
          //status
          $std_ss = StudentStatus::where("StudentID",$sId)->get();
          if(!empty($std_ss)){
            StudentStatus::where("StudentID",$sId)->delete();
          }
        if(isset($request->status))
         {
          foreach($request->status as $stts)
          {
            $sts = json_decode($stts);
            $status = new StudentStatus();
            $status->StudentID=$std->ID;
            $status->StudentStatusID= $sts->id;
            $status->StudentStatusDate=$sts->date;
            $status->StudentStatusComment=$sts->note;
            $status->save();
          }
        }
        //courses in grade
        if(isset($request->course))
         {
          Grade::where("StudentID",$sId)->delete();
          foreach($request->course as $c)
          {
              $grade = new Grade();
              $crs = json_decode($c);
            $grade->StudentID= $std->ID;
            $grade->CourseID= $crs->CourseID;
            $grade->FinalExameDate=$crs->FinalExamDate;
            $grade->Enrolled= $crs->Enrolled;
            $grade->Percentage= $crs->Percentage;
            $grade->DescriptiveGrade= $crs->DescGrade;
            $grade->Grade=  $crs->Grade;
            $grade->ECTSGrade= $crs->ECTSGrade;
            $grade->save();
          }
        }
        //business
        $stdBussiness = json_decode($request->stdBussiness);
         $business =Business::where("StudentID",$sId)->first();
        $business->StudentID=$std->ID;
        $business->ContactPersonTitle=$stdBussiness->ContactPersonTitle;
        $business->ContactPersonLastName=$stdBussiness->ContactPersonLastName;
        $business->CompanyName=$stdBussiness->CompanyName;
        $business->Department=$stdBussiness->Department;
        $business->Position=$stdBussiness->Position;
        $business->StreetAddress=$stdBussiness->StreetAddress;
        $business->Zip=$stdBussiness->Zip;
        $business->Phone=$stdBussiness->Phone;
        $business->Email=$stdBussiness->Email;
        $business->Fax=$stdBussiness->Fax;
        $business->Notes=$stdBussiness->Notes;
        $business->Mobile=$stdBussiness->Mobile;
        $business->Country=$stdBussiness->Country;
        $business->City=$stdBussiness->City;
        $business->WebAddress=$stdBussiness->WebAddress;
        $business->ContactPersonFirstName=$stdBussiness->ContactPersonFirstName;
        $business->ContactPersonDepartment=$stdBussiness->ContactPersonDepartment;
        $business->save();
         // attachments part
        if(isset($request->std_attached_files))
        {
            Attachment::where("StudentID",$sId)->delete();
         foreach($request->std_attached_files as $at)
         {
            $attach = json_decode($at);
            $attachment = new Attachment();
            $attachment->File =$attach->id.'/'.$attach->name;
            $attachment->StudentID = $std->ID;
            $attachment->Description = $attach->des;
            $attachment->save();
         }
       }

        /// addmission data
        $stdAddmission= json_decode($request->stdAdmission);
        $stdAdm = Admission::where("StudentID",$sId)->first();
        $stdAdm->EducationalBackground=$stdAddmission->EducationalBackground;
        $stdAdm->SBSTest=$stdAddmission->SBSTest;
        $stdAdm->GMATTest=$stdAddmission->GMATTest;
        $stdAdm->GMATResult=$stdAddmission->GMATResult;
        $stdAdm->SATTest=$stdAddmission->SATTest;
        $stdAdm->SATResult=$stdAddmission->SATResult;
        $stdAdm->TOEFLTest=$stdAddmission->TOEFLTest;
        $stdAdm->TOEFLResult=$stdAddmission->TOEFLResult;
        $stdAdm->IELTSTest=$stdAddmission->IELTSTest;
        $stdAdm->IELTSResult=$stdAddmission->IELTSResult;
        $stdAdm->ExamAdmBBA=$stdAddmission->ExamAdmBBA;
        $stdAdm->ExamAdmResultBBA=$stdAddmission->ExamAdmResultBBA;
        $stdAdm->ExamEntBBA=$stdAddmission->ExamEntBBA;
        $stdAdm->ExamEntResultBBA=$stdAddmission->ExamEntResultBBA;
        $stdAdm->ExamOthBBA=$stdAddmission->ExamOthBBA;
        $stdAdm->ExamOthResultBBA=$stdAddmission->ExamOthResultBBA;
        $stdAdm->ExamAdmMAS=$stdAddmission->ExamAdmMAS;
        $stdAdm->ExamAdmResultMAS=$stdAddmission->ExamAdmResultMAS;
        $stdAdm->ExamEntMAS=$stdAddmission->ExamEntMAS;
        $stdAdm->ExamEntResultMAS=$stdAddmission->ExamEntResultMAS;
        $stdAdm->ExamOthMAS=$stdAddmission->ExamOthMAS;
        $stdAdm->ExamOthResultMAS=$stdAddmission->ExamOthResultMAS;
        $stdAdm->ExamEntDBA=$stdAddmission->ExamEntDBA;
        $stdAdm->ExamEntResultDBA=$stdAddmission->ExamEntResultDBA;
        $stdAdm->ExamOthDBA=$stdAddmission->ExamOthDBA;
        $stdAdm->ExamOthResultDBA=$stdAddmission->ExamOthResultDBA;
        $stdAdm->StudentID=$std->ID;
        $stdAdm->save();

        // return response()->json([
        // 'Admission'=>$stdAdm,
        // 'Bussiness'=>$business,
        // 'Student'=>$std,
        // 'attachment'=>$attachment,
        // 'grade'=>$grade,
        // 'status'=>$status,
        // 'graduation'=>$graduation,
        // 'program'=>$s_p,
        // 'message'=>"Student Created successfully"
        // ]);

        return response()->json("success");
    }


    public   function deleteAttachFile(Request $request){

        if(isset($request->fId) ){
            if($request->fId !=null)
            Attachment::where("ID",$request->fId)->delete();
        }

        \File::deleteDirectory(public_path('/student_attached_files'.'/'.$request->id));

        return response()->json('success');
    }

    public function editStudent($id)
    {
        $student = Student::where("ID",$id)->first();
        $stdBussiness = Business::where("StudentID",$id)->first();
        $stdAddmission =Admission::where("StudentID",$id)->first();
        $stdProgram =StudentProgram::where("StudentID",$id)->first();
        $stdGrade =Grade::where("StudentID",$id)->get();
        $stdGradutionReq =GradutionReq::where("StudentID",$id)->first();
        $stdStatus =StudentStatus::where("StudentID",$id)->get();
        $stdAttachment =Attachment::where("StudentID",$id)->get();
        $all_course =Course::all();


        return response()->json(['all_courses'=>$all_course,'student'=>$student,'stdBussiness'=>$stdBussiness,'stdAddmission'=>$stdAddmission,
        'stdProgram'=>$stdProgram,'stdGrade'=>$stdGrade,'stdGradutionReq'=>$stdGradutionReq,'stdStatus'=>$stdStatus,'stdAttachment'=>$stdAttachment]);

    }

    public function getStudentNumber(){

        $num = Student::orderBy('ID', 'desc')->first();
        if($num == null)
        return response()->json(1);
        else
        return response()->json($num->StudentNumber+1);
    }
    public function destroy($id)
    {

        Student::where("ID",$id)->delete();
        return response()->json([
            'message' => 'Deleted Successfully'
        ], 200);

    }

    public function search(Request $request)
    {
        $student = Student::Where('FirstName','LIKE','%'.$request->search.'%')->orWhere('LastName','LIKE','%'.$request->search.'%')->paginate(20);
        $response = [
            'pagination' => [
                'total' => $student->total(),
                'per_page' => $student->perPage(),
                'current_page' => $student->currentPage(),
                'last_page' => $student->lastPage(),
                'from' => $student->firstItem(),
                'to' => $student->lastItem()
            ],
            'data' => $student
        ];
        return response()->json($response);
    }
    public function attachStudentFile(Request $request)
    {
        if($request->file !='')
        {
            $uId = uniqid();
            $ext = $request->file->getClientOriginalExtension();
            $filename =$request->file->getClientOriginalName();
            $request->file->move(public_path('/student_attached_files'.'/'.$uId),$filename);
            return response()->json(['filename'=>$filename,'extension'=>$ext,'id'=>$uId]);
        }
        else
        {
            return response()->json('failed');

        }
    }
    public function searchnumber(Request $request)
    {
        // $student = Student::Where('StudentNumber','LIKE','%'.$request->search_number.'%')->paginate(10);

        $student =  Student::Where('StudentNumber','LIKE','%'.$request->search_number.'%')->orderBy('ID','desc')->with('partner')->with("program")->with("currentStatus")->paginate(10);

        $response = [
            'pagination' => [
                'total' => $student->total(),
                'per_page' => $student->perPage(),
                'current_page' => $student->currentPage(),
                'last_page' => $student->lastPage(),
                'from' =>

                $student->firstItem(),
                'to' => $student->lastItem()
            ],
            'data' => $student
        ];
        return response()->json($response);
    }

    public function studentndex()
    {
        return view('students.index');
    }
    public function studentcreate()
    {
        return view('students.home');
    }

    public function updatestd()
    {
      return view('students.index');
    }
    public function getAllStudents()
    {
        return response()->json(Student::orderBy('ID','desc')->get());

    }

    public function getAllStatus()
    {
        return response()->json(Status::all());
    }

    public function getAllCountries()
    {
        return response()->json(DB::table('country')->orderBy('printable_name', 'desc')->get());
    }

public function invoiceCreate(Request $request)
{
    $invoice= new Invoice();
    $invoice->InvoicePurpose=$request->InvoicePurpose;
    $invoice->InvoiceNumber=$request->InvoiceNumber;
    $invoice->Ammount=$request->Ammount;
    $invoice->Currency=$request->Currency;
    $invoice->InvoiceDate=$request->InvoiceDate;
    $invoice->DueDate=$request->DueDate;
    $invoice->PaymentDate=$request->PaymentDate;
    $invoice->Paid=$request->Paid;
    $invoice->save();

    if($request->StudentID!=null){
        $studentinvoice= new StudentInvoice();
        $studentinvoice->StudentID=$request->StudentID;
        $studentinvoice->ID=$invoice->ID;
        $studentinvoice->save();
    }
    return response()->json("Invoice Create Seccessfully");
}
public function invoiceUpdate(Request $request, $id)
{

    $invoice= Invoice::where('ID',$id)->first();
    $invoice->InvoicePurpose=$request->InvoicePurpose;
    $invoice->InvoiceNumber=$request->InvoiceNumber;
    $invoice->Ammount=$request->Ammount;
    $invoice->Currency=$request->Currency;
    $invoice->InvoiceDate=$request->InvoiceDate;
    $invoice->DueDate=$request->DueDate;
    $invoice->PaymentDate=$request->PaymentDate;
    $invoice->Paid=$request->Paid;
    $invoice->save();
    return response()->json("Updated Successfully");
}
}
