<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Book;
use App\BookBundle;
use Illuminate\Support\Facades\DB;
class BookController extends Controller
{

    public function saveBook(Request $request)
    {

        $cover =null;
        if($request->Cover !='')
        {
            $cover = $request->Cover->getClientOriginalName();
            $request->Cover->move(public_path('/bookCover'),$cover);
        }
        $book = new Book();
        $book->Title =  $request->Title;
        $book->Author =$request->Author;
        $book->Publisher = $request->Publisher;
        $book->Edition = $request->Edition;
        $book->ISBN =$request->ISBN;
        $book->Year = $request->Year;
        $book->Cover = $cover;
        $book->save();

        if(isset($request->courses))
        {
            foreach($request->courses as $course)
            {
                $crs = json_decode($course);
                DB::table('coursebook')->insert([
                    'CourseID'=>$crs->ID,
                    'BookID'=> $book->id,
                ]);
            }
        }
       return  response()->json(['book'=>$book,'message'=>"Book CReatedd Successfully"]);
    }

    /// get book bundle list and book list
    public function getBookBundleList()
    {
        return response()->json(BookBundle::all());
    }
    
    public function getBookList()
    {
        return response()->json(Book::all());
    }

    public function getBookByTitle(Request $request)
    {
        return response()->json(Book::where('Title','LIKE','%'.$request->book.'%')->get());
    }
    public function saveBookBundle(Request $request)
    {
        $bookBundle =json_decode($request->bookBundle);
        $bundle = new BookBundle();
        $bundle->Description = $bookBundle->description;
        $bundle->CreatedOn = $bookBundle->creationDate;
        $bundle->save();

        if(isset($request->books)){
            
        foreach($request->books as $book)
        {
            $b = json_decode($book);
            DB::table('bbelong')->insert([
                'BookID'=>$b->id,
                'BookBundleID'=>$bundle->ID,
            ]);
        }
        }
        return response()->json('success');
    }
    public function editBookBundle($id){
        // $books =  DB::table('bbelong')->where('BookBundleID', $id)->get();
        $bookBundle = BookBundle::where("ID",$id)->first();
        $books =DB::table('bbelong')->join('book','bbelong.BookID','=','book.ID')->select('bbelong.*','book.Title')->where("BookBundleID",$id)->get();

        return response()->json(['books'=>$books,'bookBundle'=>$bookBundle]);

    }
    // update book bundle

    public  function updateBookBundle(Request $request,$id)
    {

        
        $bookBundle =json_decode($request->bookBundle);
        $bundle =BookBundle::where("ID",$id)->first();
        $bundle->Description = $bookBundle->Description;
        $bundle->CreatedOn = $bookBundle->CreatedOn;
        $bundle->save();

        if(!empty(DB::table("bbelong")->where("BookBundleID",$id)->get())){
            DB::table("bbelong")->where("BookBundleID",$id)->delete();
        }

        if(isset($request->books)){
        foreach($request->books as $book)
        {
            $b = json_decode($book);
            DB::table('bbelong')->insert([
                'BookID'=>$b->id,
                'BookBundleID'=>$bundle->ID,
            ]);
        }
        }
        return response()->json('success');
    }
    public function searchBookByTitle(Request $request)
    {
        $book = Book::Where('Title','LIKE','%'.$request->book_title.'%')->paginate(10);
        $response = [
            'pagination' => [
                'total' => $book->total(),
                'per_page' => $book->perPage(),
                'current_page' => $book->currentPage(),
                'last_page' => $book->lastPage(),
                'from' => $book->firstItem(),
                'to' => $book->lastItem()
            ],
            'data' => $book
        ];
        return response()->json($response);
    }
    public function searchBundleDescription(Request $request)
    {
        $bookbundle = BookBundle::Where('Description','LIKE','%'.$request->bundle_description.'%')->paginate(10);
        $response = [
            'pagination' => [
                'total' => $bookbundle->total(),
                'per_page' => $bookbundle->perPage(),
                'current_page' => $bookbundle->currentPage(),
                'last_page' => $bookbundle->lastPage(),
                'from' => $bookbundle->firstItem(),
                'to' => $bookbundle->lastItem()
            ],
            'data' => $bookbundle
        ];
        return response()->json($response);
    }
    public function fetchBookBundleByID($id)
    {
        $bookBundle=BookBundle::where('ID',$id)->first();
            
        return response()->json($bookBundle);
    }
    public function getBookByID($id)
    {
        $book = Book::where("ID",$id)->first();
        $course =DB::table('coursebook')->join('course','coursebook.CourseID','=','course.ID')->select('course.*','course.ID')->where("ID",$id)->get();
        return response()->json(['books'=>$book,'course'=>$course]);
    }
   
    public function updateBook(Request $request,$id)
    {
        $cover =null;
        if($request->Cover !='')
        {
            $cover = $request->Cover->getClientOriginalName();
            $request->Cover->move(public_path('/bookCover'),$cover);
        }
      
        $book =Book::where("ID",$id)->first();
        $book->Title =  $request->Title;
        $book->Author =$request->Author;
        $book->Publisher = $request->Publisher;
        $book->Edition = $request->Edition;
        $book->ISBN =$request->ISBN;
        $book->Year = $request->Year;
        $book->Cover = $cover;
        $book->save();
        return response()->json($book); 
    //     if(!empty(DB::table("coursebook")->where("BookID",$id)->get())){
    //         DB::table("coursebook")->where("BookID",$id)->delete();
    //     }
    //     if(isset($request->courses))
    //     {
    //         foreach($request->courses as $course)
    //         {
    //             $crs = json_decode($course);
    //             DB::table('coursebook')->insert([
    //                 'CourseID'=>$crs->ID,
    //                 'BookID'=> $book->id,
    //             ]);
    //         }
    // }
    
}
public function deleteBook($id)
{
    $book=Book::where('ID',$id)->first();
    $book->delete();
    return response()->json("deleted");
}
}
