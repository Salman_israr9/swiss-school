<?php

namespace App\Http\Controllers\partner;

use App\partner\Partner;
use App\partner\Partnercampus;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use App\status\Status;
use App\Program;
use App\Country;


class PartnerController extends Controller
{
    


    public function getFillterList()
    {
        return response()->json(['programList'=>Program::all(),'statusList'=>Status::all(),'partnerCodeList'=>Partner::all(),'countryList'=>DB::table("country")->get()]);
        
    }
    
    public function getStatusList()
    {
        return Status::all();
        
    }
    public function getCountryList()
    {
        return DB::table("country")->get();

    }  
    public function getPartnerCodeList()
    {

        return Partner::all();
        
    }
    
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\m  $m
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, m $m)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\m  $m
     * @return \Illuminate\Http\Response
     */
    public function destroy(m $m)
    {
        //
    }

    public function getAllPartner()
    {
        return response()->json(Partner::all());
    }

    public function getAllPartnerCampus(Request $request)
    {
        // partnerCampId
        return response()->json(Partnercampus::where('PartnerID',$request->partnerCampId)->where('PartnerCampusActive',1)->get());
    }
}
