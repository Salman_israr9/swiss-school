<?php

namespace App\Http\Controllers;
use App\Program;
use Illuminate\Http\Request;
use App\Book;
use App\BookBundle;
use App\Course;
use App\Invoice;
use App\StudentInvoice;
use App\students\Student;
use App\Company;
use App\CompanyInvoice;
use Illuminate\Support\Facades\DB;

class ProgramController extends Controller
{

    public function getData()
    {
        return    Program::with("course")->paginate(2);
    }
    public function index()
    {
        $program = DB::table('program')->orderBy('AcademicYear', 'desc')->paginate(10);
        $response = [
            'pagination' => [
                'total' => $program->total(),
                'per_page' => $program->perPage(),
                'current_page' => $program->currentPage(),
                'last_page' => $program->lastPage(),
                'from' => $program->firstItem(),
            'to' => $program->lastItem()
            ],
            'data' => $program
        ];
        return response()->json($response);

    }
    public function getBookBoundle()
    {
        $program = BookBundle::paginate(10);
        $response = [
            'pagination' => [
                'total' => $program->total(),
                'per_page' => $program->perPage(),
                'current_page' => $program->currentPage(),
                'last_page' => $program->lastPage(),
                'from' => $program->firstItem(),
            'to' => $program->lastItem()
            ],
            'data' => $program
        ];
        return response()->json($response);

    }

    public function getCourseList()
    {

        $data = Course::paginate(10);
        $response = [
            'pagination' => [
                'total' => $data->total(),
                'per_page' => $data->perPage(),
                'current_page' => $data->currentPage(),
                'last_page' => $data->lastPage(),
                'from' => $data->firstItem(),
            'to' => $data->lastItem()
            ],
            'data' => $data
        ];
        return response()->json($response);

    }

    // search program
    public function seachProgram(Request $request)
    {
     return response()->json(Program::select('ID','ProgramTitle')->where('ProgramTitle','like','%'.$request->search_data.'%')->get());
    }
    

    // search course
    public function searchCourse(Request $req)
    {
        $searchquery = $req->search_data;
        $data = Course::where('CourseTitle','like','%'.$searchquery.'%')->get();
        return response()->json($data);
    }


    // invoice
    public function searchInoiceByNumber(Request $request)
    {
        $invoice = Invoice::Where('InvoiceNumber','LIKE','%'.$request->search.'%')->paginate(10);
        $response = [
            'pagination' => [
                'total' => $invoice->total(),
                'per_page' => $invoice->perPage(),
                'current_page' => $invoice->currentPage(),
                'last_page' => $invoice->lastPage(),
                'from' =>
                $invoice->firstItem(),
                'to' => $invoice->lastItem()
            ],
            'data' => $invoice
        ];

        return response()->json($response);
    }


    public function getBooks()
    {
        $program = Book::paginate(10);
        $response = [
            'pagination' => [
                'total' => $program->total(),
                'per_page' => $program->perPage(),
                'current_page' => $program->currentPage(),
                'last_page' => $program->lastPage(),
                'from' => $program->firstItem(),
            'to' => $program->lastItem()
            ],
            'data' => $program
        ];
        return response()->json($response);

    }

    public function getInvoiceList()
    {

        $data = Invoice::paginate(10);

        $response = [
            'pagination' => [
                'total' => $data->total(),
                'per_page' => $data->perPage(),
                'current_page' => $data->currentPage(),
                'last_page' => $data->lastPage(),
                'from' => $data->firstItem(),
            'to' => $data->lastItem()
            ],
            'data' => $data
        ];
        return response()->json($response);

    }

    public function getStudentInvoiceList()
    {
       
       
       
        $data =  DB::table('sinvoice')
        ->join('invoice', 'sinvoice.ID', '=', 'invoice.ID')
        ->join('student','sinvoice.StudentID','=','student.ID')
        ->select('sinvoice.StudentID','student.*','invoice.*')
        ->paginate(10);    
        $response = [
            'pagination' => [
                'total' => $data->total(),
                'per_page' => $data->perPage(),
                'current_page' => $data->currentPage(),
                'last_page' => $data->lastPage(),
                'from' => $data->firstItem(),
            'to' => $data->lastItem()
            ],
            'data' => $data
        ];
            return response()->json($response);
    }

    public function getInvoiceByID($id)
    {
       $data =  DB::table('sinvoice')
        ->join('invoice', 'sinvoice.ID', '=', 'invoice.ID')->where('invoice.ID',$id)
        ->join('student','sinvoice.StudentID','=','student.ID')
        ->select('sinvoice.StudentID','student.*','invoice.*')->get() ;
        return response()->json($data);
    }
    public function getStudentPaidInvoice()
    {
        
        $data =  DB::table('sinvoice')
        ->join('invoice', 'sinvoice.ID', '=', 'invoice.ID')->where('invoice.Paid','!=',-1)
        ->select('sinvoice.StudentID', 'invoice.*')
        ->paginate(10);
        $response = [
            'pagination' => [
                'total' => $data->total(),
                'per_page' => $data->perPage(),
                'current_page' => $data->currentPage(),
                'last_page' => $data->lastPage(),
                'from' => $data->firstItem(),
            'to' => $data->lastItem()
            ],
            'data' => $data
        ];
        return response()->json($response);
    }
    public function getStudentUnpaidInvoice()
    {
        $data =  DB::table('sinvoice')
        ->join('invoice', 'sinvoice.ID', '=', 'invoice.ID')->where('invoice.Paid','=',-1)
        ->join('student','sinvoice.StudentID','=','student.ID')
        ->select('sinvoice.StudentID', 'invoice.*','student.*')
        ->paginate(10);
       $response = [
        'pagination' => [
            'total' => $data->total(),
            'per_page' => $data->perPage(),
            'current_page' => $data->currentPage(),
            'last_page' => $data->lastPage(),
            'from' => $data->firstItem(),
        'to' => $data->lastItem()
        ],
        'data' => $data
    ];
    return response()->json($response);
    }

    public function getCompanyInvoiceList()
    {

        $data =  DB::table('cinvoice')
        ->join('invoice', 'cinvoice.ID', '=', 'invoice.ID')
        ->join('company','cinvoice.CompanyID','=','company.ID')
        ->select('cinvoice.CompanyID', 'invoice.*','company.CompanyName')
        ->paginate(6);
        $response = [
            'pagination' => [
                'total' => $data->total(),
                'per_page' => $data->perPage(),
                'current_page' => $data->currentPage(),
                'last_page' => $data->lastPage(),
                'from' => $data->firstItem(),
            'to' => $data->lastItem()
            ],
            'data' => $data
        ];
        return response()->json($response);
    }

    public function getCompanyPaidInvoiceList()
    {
             $data =  DB::table('cinvoice')
            ->join('invoice', 'cinvoice.ID', '=', 'invoice.ID')->where('invoice.Paid','!=',-1)
            ->select('cinvoice.CompanyID', 'invoice.*')
            ->paginate(10);
        $response = [
            'pagination' => [
                'total' => $data->total(),
                'per_page' => $data->perPage(),
                'current_page' => $data->currentPage(),
                'last_page' => $data->lastPage(),
                'from' => $data->firstItem(),
            'to' => $data->lastItem()
            ],
            'data' => $data
        ];
        return response()->json($response);
    }


    /// get student contacts

    public function getProgramStudentContacts($id)
    {
        $program = Program::where("ID",$id)->with('student')->paginate(10);
      
        $response = [
            'pagination' => [
                'total' => $program->total(),
                'per_page' => $program->perPage(),
                'current_page' => $program->currentPage(),
                'last_page' => $program->lastPage(),
                'from' => $program->firstItem(),
            'to' => $program->lastItem()
            ],
            'data' => $program
        ];
        return response()->json([$response]);
    }
    public function getCompanyUnpaidInvoiceList()
    {
        $data =  DB::table('cinvoice')
        ->join('invoice', 'cinvoice.ID', '=', 'invoice.ID')->where('invoice.Paid','=',-1)
        ->join('company','cinvoice.CompanyID','=','company.ID')
        ->select('cinvoice.CompanyID', 'invoice.*','company.CompanyName')
        ->paginate(10);
        $response = [
            'pagination' => [
                'total' => $data->total(),
                'per_page' => $data->perPage(),
                'current_page' => $data->currentPage(),
                'last_page' => $data->lastPage(),
                'from' => $data->firstItem(),
            'to' => $data->lastItem()
            ],
            'data' => $data
        ];
        return response()->json($response);
    }

    public   function getProgramName($id){

        $p = Program::where("ID",$id)->first();
        return response()->json($p);
    }

public function createCompany(Request $request)
{    
    $comp= new Company();

    $comp->CompanyName =$request->CompanyName;
    $comp->Department =$request->Department;
    $comp->ContactPersonTitle =$request->ContactPersonTitle;
    $comp->ContactPersonFirstName =$request->ContactPersonFirstName;
    $comp->ContactPersonLastName =$request->ContactPersonLastName;
    $comp->StreetAddress =$request->StreetAddress;
    $comp->City =$request->City;
    $comp->Zip =$request->Zip;
    $comp->Country =$request->Country;
    $comp->Phone =$request->Phone;
    $comp->Mobile =$request->Mobile;
    $comp->Fax =$request->Fax;
    $comp->Email =$request->Email;
    $comp->Email2 =$request->Email2;
    $comp->Email3 =$request->Email3;
    $comp->WebAddress =$request->WebAddress;
    $comp->Notes =$request->Notes;

    $comp->save();
    return response()->json($comp);
}

public function getcompanybyID($id)
{
    $company = Company::where('ID', $id)->first();
    return response()->json($company);
}


public function updateCompany(Request $request, $id)
{
    
    $comp=  Company::where('ID', $id)->first();
    $comp->CompanyName =$request->CompanyName;
    $comp->Department =$request->Department;
    $comp->ContactPersonTitle =$request->ContactPersonTitle;
    $comp->ContactPersonFirstName =$request->ContactPersonFirstName;
    $comp->ContactPersonLastName =$request->ContactPersonLastName;
    $comp->StreetAddress =$request->StreetAddress;
    $comp->City =$request->City;
    $comp->Zip =$request->Zip;
    $comp->Country =$request->Country;
    $comp->Phone =$request->Phone;
    $comp->Mobile =$request->Mobile;
    $comp->Fax =$request->Fax;
    $comp->Email =$request->Email;
    $comp->Email2 =$request->Email2;
    $comp->Email3 =$request->Email3;
    $comp->WebAddress =$request->WebAddress;
    $comp->Notes =$request->Notes;

    $comp->save();
    return response()->json($comp);
}

 public function deletecompany($id)
 {
    $comp=  Company::where('ID', $id)->first();
    $comp->delete();
    return response()->json($comp);
 }
 
 public function searchAcademic(Request $request)
 {
    return response()->json(Program::select('ID','AcademicYear')->where('AcademicYear','like','%'.$request->search_data.'%')->get());

 }

 public function getmbacourse()
 {
     $mbacourse = Program::where('ProgramCode','mba')->get();
     return response()->json($mbacourse);
 }
 
 public function saveProgram(Request $request)
 {
    $program= new Program();
      $pro= json_decode($request->program); 
    // // return response()->json($pro);
    
     $program->ProgramTitle= $pro->ProgramTitle;
     $program->ProgramCode= $pro->ProgramCode;
     $program->ProgramType= $pro->ProgramType;
     $program->AcademicYear= $pro->AcademicYear;
     $program->Notes= $pro->Notes;
     $program->save();


     if(isset($request->courses))
     {
      foreach($request->courses as $course)
      {
        $crs = json_decode($course);
        DB::table('belong')->insert([
            'CourseID'=>$crs->ID,
            'Semester'=> $crs->Term,
            'ProgramYear'=> $crs->ProgramYear,
            'ProgramID'=>$program->ID
        ]);
      }
    }		
    return response()->json("success");	

 }
 
 public function searchProgramByTitle(Request $request)
 {
        $program = Program::Where('ProgramTitle','LIKE','%'.$request->program_title.'%')->paginate(10);
        $response = [
            'pagination' => [
                'total' => $program->total(),
                'per_page' => $program->perPage(),
                'current_page' => $program->currentPage(),
                'last_page' => $program->lastPage(),
                'from' => $program->firstItem(),
                'to' => $program->lastItem()
            ],
            'data' => $program
        ];
        return response()->json($response);
    }

    public function searchByCompanyName(Request $request)
    {
        $company = Company::Where('CompanyName','LIKE','%'.$request->company_name.'%')->paginate(20);
        $response = [
            'pagination' => [
                'total' => $company->total(),
                'per_page' => $company->perPage(),
                'current_page' => $company->currentPage(),
                'last_page' => $company->lastPage(),
                'from' => $company->firstItem(),
                'to' => $company->lastItem()
            ],
            'data' => $company
        ];
        return response()->json($response);
    }

    public function showTranscript()
    {
     return view('students.trans_std');
    }

    public function getProgramByID($id)
    {
        $data = DB::table('belong')
            ->join('program', 'belong.ProgramID', '=', 'program.ID')->where('program.ID', $id)
            ->join('course', 'belong.CourseID', '=', 'Course.ID')
            ->select('belong.ProgramID', 'program.*', 'course.*')->first();
        return response()->json($data); 
    }
 }

