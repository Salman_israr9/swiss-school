<?php

namespace App\Http\Controllers;

use App\Course;
use App\students\Student;
use App\BookBundle;
use App\Book;
use App\Professor;
use App\Invoice;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class CourseController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
       // return Student::where("ID",1907)->first()->bookbundle;
      // return  Professor::with('student')->get();

      return  DB::table('invoice')->leftJoin('sinvoice','invoice.ID','=','sinvoice.ID')->get();


    //   DB::table('student')
    //         ->whereExists(function($query)
    //         {
    //             $query->select(DB::raw()->all())
    //                   ->from('sinvoice')
    //                   ->whereRaw('sinvoice.StudentID' = 'student.ID');
    //         })
    //         ->get();

    //   return $pets = Student::with('invoice')->whereHas('invoice', function($query) use ($id) {
    //     $query->where('student.ID', '!=', $id);
    //   })->get();

      $data = Invoice::paginate();
              $response = [
            'pagination' => [
                'total' => $data->total(),
                'per_page' => $data->perPage(),
                'current_page' => $data->currentPage(),
                'last_page' => $data->lastPage(),
                'from' => $data->firstItem(),
            'to' => $data->lastItem()
            ],
            'data' => Student::all()
        ];
        return response()->json($response);
      return $d;
       return  Student::with('invoice')->get();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Course  $programCourse
     * @return \Illuminate\Http\Response
     */
    public function show(Course $Course)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Course  $programCourse
     * @return \Illuminate\Http\Response
     */
    public function edit(Course $Course)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Course  $Course
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Course $Course)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Course  $Course
     * @return \Illuminate\Http\Response
     */
    public function destroy(Course $Course)
    {
        //
    }

    public function searchcode(Request $request)
    {
        $course = Course::Where('CourseCode','LIKE','%'.$request->search_code.'%')->paginate(10);
        $response = [
            'pagination' => [
                'total' => $course->total(),
                'per_page' => $course->perPage(),
                'current_page' => $course->currentPage(),
                'last_page' => $course->lastPage(),
                'from' => $course->firstItem(),
                'to' => $course->lastItem()
            ],
            'data' => $course
        ];
        return response()->json($response);
    }

    public function searchtitle(Request $request)
    {
        $course = Course::Where('CourseTitle','LIKE','%'.$request->search_title.'%')->paginate(10);
        $response = [
            'pagination' => [
                'total' => $course->total(),
                'per_page' => $course->perPage(),
                'current_page' => $course->currentPage(),
                'last_page' => $course->lastPage(),
                'from' => $course->firstItem(),
                'to' => $course->lastItem()
            ],
            'data' => $course
        ];
        return response()->json($response);
    }
   
    public function createcourse(Request $request)
    {
        $course = new Course();

        $course->CourseTitle=$request->CourseTitle;
        $course->CourseCode=$request->CourseCode;
        $course->ECTSCredits=$request->ECTSCredits;
        $course->SBSCredits=$request->SBSCredits;
        
        $course->save();
        return response()->json([$course,"Course Deleted successfully"]);
    }

    public function getCourseByID($id)
    {
        $course= Course::where('ID',$id)->first();
        return response()->json($course);
    }
    public function updateCourse($id,Request $request)
    {
        $course= Course::where('ID',$id)->first();

        $course->CourseTitle=$request->CourseTitle;
        $course->CourseCode=$request->CourseCode;
        $course->ECTSCredits=$request->ECTSCredits;
        $course->SBSCredits=$request->SBSCredits;
        
        $course->save();
        return response()->json([$course,"Course Deleted successfully"]);
    }

}
