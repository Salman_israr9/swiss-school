<?php

namespace App\Http\Controllers;
use App\User;
use jeremykenedy\LaravelRoles\Models\Role;
use jeremykenedy\LaravelRoles\Models\Permission;
use Illuminate\Http\Request;

class UserController extends Controller
{
    

    public function getUserList()
    {
        return response()->json(User::all());
    } 

    public function getRoleList()
    {
        return response()->json(Role::all());
    }

    public function getRole($id)
    {
        return response()->json(['role'=>Role::find($id),'permissions'=>Permission::all()]);
    }
    
}
