<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator;
use App\User;
use Response;
class AdminController extends Controller
{
    

    public function registerUser(Request $request)
    {
        $data =["name"=>$request->name,"email"=>$request->email,"password"=>$request->password,'password_confirmation'=>$request->password_confirmation];

                $validator = Validator::make( $data , [
                    'name' => ['required', 'string', 'max:255'],
                    'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
                    'password' => ['required', 'string', 'min:6', 'confirmed'],
                ]);
                // $validator = Validator::make(Input::all(), $rules);

                // Validate the input and return correct response
                if ($validator->fails())
                {
                    return response()->json(array(
                        'success' => false,
                        'errors' => $validator->getMessageBag()->toArray()
                    ), 200);
                   // return Response::json(); // 400 being the HTTP code for an invalid request.
                }

                $user = new User();
                $user->email = $request->email;
                $user->name = $request->name;
                $user->password = bcrypt($request->password);
                $user->save();
                
                return Response::json(array('success' => true), 200);
    }
}
