<?php
namespace App\Http\Controllers;
use App\Company;
use App\Invoice;
use App\CompanyInvoice;
use Illuminate\Http\Request;
class CompanyController extends Controller
{

    public function index()
    {
        $company = Company::paginate(10);
        $response = [
            'pagination' => [
                'total' => $company->total(),
                'per_page' => $company->perPage(),
                'current_page' => $company->currentPage(),
                'last_page' => $company->lastPage(),
                'from' => $company->firstItem(),
            'to' => $company->lastItem()
            ],
            'data' => $company
        ];
        return response()->json($response);
    }

    /// save company invoice
    public function companyInvoiceCreate(Request $request)
    {
        
    $invoice= new Invoice();
    $invoice->InvoicePurpose=$request->InvoicePurpose;
    $invoice->InvoiceNumber=$request->InvoiceNumber;
    $invoice->Ammount=$request->Ammount;
    $invoice->Currency=$request->Currency;
    $invoice->InvoiceDate=$request->InvoiceDate;
    $invoice->DueDate=$request->DueDate;
    $invoice->PaymentDate=$request->PaymentDate;
    $invoice->Paid=$request->Paid;
    $invoice->save();
    if($request->CompanyID != null)
    {
        $c_invoice = new CompanyInvoice();
        $c_invoice->CompanyID= $request->CompanyID;
        $c_invoice->ID = $invoice->ID;
        $c_invoice->save();
    }
    return response()->json('success');
    }
    public function getCompanyByName(Request $request)
    {

        return Company::where('CompanyName','LIKE','%'.$request->name.'%')->get();
    }
    public function search(Request $request)
    {
        $company = Company::Where('CompanyName','LIKE','%'.$request->search.'%')->orWhere('Country','LIKE','%'.$request->search.'%')->paginate(1);
        $response = [
            'pagination' => [
                'total' => $company->total(),
                'per_page' => $company->perPage(),
                'current_page' => $company->currentPage(),
                'last_page' => $company->lastPage(),
                'from' => $company->firstItem(),
                'to' => $company->lastItem()
            ],
            'data' => $company
        ];
        return response()->json($response);
    }
}
