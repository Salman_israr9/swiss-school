<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CompanyInvoice extends Model
{
    protected $table='cinvoice';
    protected $fillable= ['CompanyID','ID','TS'];
    public $timestamps=false;
    
    
    // public function invoice(){
    //     return $this->hasOne('App\Invoice','','');
    // }

}
