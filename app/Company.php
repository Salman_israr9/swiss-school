<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Company extends Model
{
    protected $primaryKey ='ID';
    protected $table="company";
    protected $fillable = [
            'CompanyName',
            'Department',
            'ContactPersonTitle',
            'ContactPersonFirstName',
            'ContactPersonLastName',
            'StreetAddress',
            'City',
            'Zip',
            'Country',
            'Phone',
            'Mobile',
            'Fax',
            'Email',
            'Email2',
            'Email3',
            'WebAddress',
            'Notes'
            ];
    public $timestamps = false;

    public function invoice()
    {
        return $this->belongsToMany('App\Invoice', 'cinvoice', 'CompanyID', 'ID');
    }

}
