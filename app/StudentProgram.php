<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class StudentProgram extends Model
{
    protected $table ='graduate';
    protected $fillable =[
        'StudentID',
        'ProgramID',
        'Program2ID',
        'BooksIncluded',
        'Graduate',
        'Withdraw',
        'GraduationDate',
        'ThesisPercentage',
        'ThesisDescriptiveGrade',
        'ThesisGrade',
        'ThesisECTSGrade',
        'ThesisSubject',
        'ThesisMentor',
        'Examiner1',
        'Examiner2',
        'Examiner3',
    ];
    public $timestamps = false;

    public function student(){
        return $this->hasOne('App\students\Student','ProgramID','StudnetID')->with("currentStatus")->orderBy('ID','desc');
    }
}
