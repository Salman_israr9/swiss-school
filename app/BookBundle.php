<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BookBundle extends Model
{
    protected $table="bookbundle";
    protected $primaryKey ='ID';
    public $timestamps = false;
    protected $fillable =['ID','Description','CreatedOn'];

    public function student()
    {
        return $this->belongsToMany('App\students\Student', 'bbbelong', 'BookBundleID', 'StudentID');
    }

    public function book()
    {
        return $this->belongsToMany('App\Book', 'bbelong','BookBundleID', 'BookID');
    }

}
