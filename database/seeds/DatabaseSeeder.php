<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {


         // Disable all mass assignment restrictions
           
        // UserFactory::create()
         //  $this->call(StudentTableSeeder::class);
        
         //   Re enable all mass assignment restrictions
       DB::table('users')->insert([
           'id'=>1,
           'name'=>'Admin',
           'email'=>'admin@admin.com',
           'password'=>bcrypt('admin')
       ]);


    }
}
