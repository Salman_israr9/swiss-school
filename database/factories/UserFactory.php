<?php

use Faker\Generator as Faker;
use App\students\Student;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$n =1;

$factory->define(App\students\Student::class, function (Faker $faker) {
   
	return [
	'StudentNumber'=>rand(),
	'FirstName'=> $faker->name,
	'LastName'=> $faker->name,
	'TranscriptName'=> $faker->name,
	'Title'=> $faker->name,
	'BirthDate'=>null,
	'BirthPlace'=> $faker->name,
	'BirthCountry'=> $faker->name,
	'StreetAddress'=> $faker->name,
	'City'=> $faker->name,
	'Zip'=>3232,
	'Country'=>1,
	'Phone'=>0,
	'Mobile'=> 1,
	'Fax'=> $faker->name,
	'Email'=>$faker->unique()->safeEmail,
	'Email2'=>$faker->unique()->safeEmail,
	'Email3'=>$faker->unique()->safeEmail,
	'WebAddress'=> $faker->name,
	'Photo'=> $faker->name,
	'Remarks'=> $faker->name,
	'Notes'=> $faker->name,
	'MailAddress'=> 1,
	'Student'=> 1,
	'Professor'=>null,
	'Staff'=>null,
	'SISEnabled'=>null,
	'GradeNotification'=>null,
	'PartnerID'=>null,
        // 'name' => $faker->name,
        // 'email' => $faker->unique()->safeEmail,
        // 'email_verified_at' => now(),
        // 'password' => '$2y$10$TKh8H1.PfQx37YgCzwiKb.KjNyWgaHb9cbcoQgdIVFlYg7B77UdFm', // secret
        // 'remember_token' => str_random(10),
	];
	
});
