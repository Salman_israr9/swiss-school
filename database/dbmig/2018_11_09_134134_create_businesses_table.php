<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBusinessTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('business', function (Blueprint $table) {
            $table->increments('ID');
            $table->integer('StudentID')->nullable();
            $table->string('CompanyName')->nullable();
            $table->string('Department')->nullable();
            $table->string('Position')->nullable();
            $table->string('StreetAddress')->nullable();
            $table->string('City')->nullable();

            $table->string('Country');
            $table->string('Phone')->nullable();
            $table->string('Mobile');
            $table->string('Zip')->nullable();
            $table->string('Fax')->nullable();
            $table->string('Email')->nullable();
            $table->string('WebAddress')->nullable();
            $table->string('ContactPersonTitle')->nullable();
            $table->string('ContactPersonFirstName')->nullable();
            $table->string('ContactPersonLastName')->nullable();
            $table->string('ContactPersonDepartment')->nullable();
            $table->string('Notes')->nullable();
            $table->string('ProfessorID')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('businesses');
    }
}
