<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAdmissionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('admissions', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('studentid');
            $table->string('admission_exam_taken');
            $table->string('educationalbackground');
            $table->string('entrance_exam_taken');
            $table->string('other');
            $table->string('m_admission_exam_taken');
            $table->string('m_entrance_exam_taken');
            $table->string('m_other');
            $table->string('doctorate_examtaken');
            $table->string('doctorate_other');
            $table->string('result_admission_b');
            $table->string('result_admission_m');
            $table->string('result_doctorate_exam');
            $table->string('result_doctorate_other');
            $table->string('result_entrance_b');
            $table->string('result_entrance_m');
            $table->string('result_other_b');
            $table->string('result_other_m');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('admissions');
    }
}
