<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePartnercampusesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('partnercampuses', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('PartnerID');
            $table->text('PartnerCampusLocation');
            $table->text('PartnerCampusActive');
            $table->text('PartnerCampusSortOrder');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('partnercampuses');
    }
}
