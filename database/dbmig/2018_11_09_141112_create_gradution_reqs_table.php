<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGradutionReqsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('gradutionreq', function (Blueprint $table) {
            $table->increments('id');
            $table->string('doc_sent_exit_survey');
            $table->string('doc_sbs_exam');
            $table->string('doc_exit_examp_regrine_taken');
            $table->string('master_exit_exam_sbs_taken');
            $table->string('master_exit_examp_regrine_taken');
            $table->string('master_exit_survey');
            $table->string('internship');
            $table->string('bachelor_exit_survey');
            $table->string('bachelore_xit_examcbetaken');
            $table->string('passing_gpa');
            $table->string('no_financial');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('gradution_reqs');
    }
}
