<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProgramsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('programs', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('studentid');
            $table->string('program');
            $table->string('secondary_major');
            $table->string('examiner_1');
            $table->string('examiner_2');
            $table->string('examiner_3');
            $table->string('thesis_grade');
            $table->string('thesis_mentor');
            $table->string('thesis_percentage');
            $table->string('thesis_subject');
            $table->string('books_included');
            $table->string('withdrawn');
            $table->string('graduated');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('programs');
    }
}
