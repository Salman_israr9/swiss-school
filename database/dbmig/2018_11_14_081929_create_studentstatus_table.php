<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStudentStatusTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('studentstatus', function (Blueprint $table) {
            $table->increments('ID');
            $table->integer('StudentStatusID');
            $table->integer('StudentID');
            $table->text('StudentStatusDate');
            $table->text('StudentStatusComment');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('studentstatus');
    }
}
