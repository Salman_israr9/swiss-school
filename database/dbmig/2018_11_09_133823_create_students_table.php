<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStudentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('students', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('StudentNumber')->nullable()->unsigned();
            $table->string('FirstName')->nullable();
            $table->string('LastName')->nullable();
            $table->string('TranscriptName')->nullable();
            $table->date('Title')->nullable();
            $table->string('BirthDate')->nullable();
            $table->string('BirthPlace')->nullable();
            $table->string('BirthCountry')->nullable();
            $table->string('StreetAddress')->nullable();
            $table->string('City')->nullable();
            $table->string('Zip')->nullable();
            $table->integer('Country')->nullable()->unsigned();
            $table->integer('Phone')->nullable()->unsigned();
            $table->string('Mobile');
            $table->string('Fax')->nullable();
            $table->string('Email')->nullable();
            $table->string('Email2')->nullable();
            $table->text('Email3')->nullable();
            $table->string('WebAddress')->nullable();
            $table->string('Photo')->nullable();
            $table->string('Remarks')->nullable();
            $table->text('Notes')->nullable();
            $table->text('MailAddress')->nullable();
            $table->string('Student')->nullable();
            $table->string('Professor')->nullable();
            $table->string('Staff')->nullable();
            $table->text('SISEnabled')->nullable();
            $table->string('GradeNotification')->nullable();
            $table->string('ImportFlag')->nullable();
            $table->string('ImportUser')->nullable();
            $table->integer('PartnerID');
            $table->integer('PartnerCampusID')->nullable();
            $table->int;

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('students');
    }
}
