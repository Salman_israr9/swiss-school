<?php

use Illuminate\Http\Request;
use App\Course;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
//transcript show
Route::get('/showTranscript','ProgramController@showTranscript');
/// create student invoice 
Route::post('/invoiceCreate',"students\StudentController@invoiceCreate");

// get last invocie
Route::get("/getInvoiceNo","students\StudentController@getInvoiceNo");
Route::post('/getStudentByName','students\StudentController@getStudentByName');
Route::post('/invoiceUpdate/{id}','students\StudentController@invoiceUpdate');
// search company by name
Route::post('/getCompanyByName','CompanyController@getCompanyByName');
// compan invoice create 
Route::post("companyInvoiceCreate",'CompanyController@companyInvoiceCreate');

Route::post('/getBookByTitle','BookController@getBookByTitle');
Route::post('/saveBookBundle','BookController@saveBookBundle');
Route::get("editBookBundle/{id}","BookController@editBookBundle");
Route::post('updateBookBundle/{id}','BookController@updateBookBundle');

/// get book bundle list and book list
Route::get("/getBookList",'BookController@getBookList');
Route::get("/getBookBundleList",'BookController@getBookBundleList');
Route::get("/fetchBookBundleByID/{id}",'BookController@fetchBookBundleByID');
// semple route
Route::get("getData","ProgramController@getData");
Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

// -----------programs routes  contact,
 Route::get("/getProgram","ProgramController@index");
Route::get('/getProgramByID/{id}', "ProgramController@getProgramByID");
 /// studnts contacts list which in porgram 

 Route::get("getProgramStudentContacts/{id}",'ProgramController@getProgramStudentContacts');
 Route::get("/getbooks","ProgramController@getBooks");
 Route::post('/saveBook','BookController@saveBook');    /// save book with course list
 Route::get("/getbookboundle","ProgramController@getBookBoundle");
 Route::get("/getStudentInvoiceList","ProgramController@getStudentInvoiceList");
 Route::get("/getInvoiceByID/{id}","ProgramController@getInvoiceByID");
 
 Route::get("/getCourseList","ProgramController@getCourseList");
 // get paid and unpaid invoice ces 
 Route::get("/getStudentPaidInvoice","ProgramController@getStudentPaidInvoice");
 Route::get("/getStudentUnpaidInvoice","ProgramController@getStudentUnpaidInvoice");
 Route::post("/searchCourse","ProgramController@searchCourse");
 Route::get("/getInvoiceList","ProgramController@getInvoiceList");
 // search invoice by number
 Route::get("searchInoiceByNumber","ProgramController@searchInoiceByNumber");
 Route::get("/getStdInvoiceList","ProgramController@getStdInvoiceList");
 Route::get("/getCompanyInvoiceList","ProgramController@getCompanyInvoiceList");
 Route::get("/getCompanyPaidInvoiceList","ProgramController@getCompanyPaidInvoiceList");
 Route::get("/getCompanyUnpaidInvoiceList","ProgramController@getCompanyUnpaidInvoiceList");
 //professor
 Route::get("/getprofessor","EmployeController@index");
 Route::get("/editProfessor/{id}","EmployeController@editProfessor");
 Route::post('updateProfessor','EmployeController@updateProfessor');
 Route::get("/searchpro","EmployeController@search");
 Route::get("/searchlastpro","EmployeController@searchlast");
 Route::get("/searchnumberpro","EmployeController@searchnumber");
 Route::post("saveProfessor",'EmployeController@saveProfessor');
 Route::get("/getpro","EmployeController@getpro");
 Route::delete("/deleteprofessor/{id}","EmployeController@destroy");

//copmany
Route::get("/getcompany","CompanyController@index");
Route::get("/searchcomp","CompanyController@search");
 //student controllers
 // attach student file or upload a student file that attached with students
 
Route::get('/getStudentNumber','students\StudentController@getStudentNumber');
Route::post('/attachStudentFile','students\StudentController@attachStudentFile');
Route::post("deleteAttachFile",'students\StudentController@deleteAttachFile');

Route::post('/createstudent','students\StudentController@store');
Route::get("/getstudent","students\StudentController@index");
Route::get("seacrhByStatus/{id}","students\StudentController@seacrhByStatus");
Route::get("/getAllStudents","students\StudentController@getAllStudents");
Route::get("/search","students\StudentController@search");
Route::get("/searchlast","students\StudentController@searchlast");
Route::get("/searchnumber","students\StudentController@searchnumber");
//Route::post('/check','students\StudentController@create');
Route::get('index', 'students\StudentController@studentindex');
Route::get('/studentcreate', 'students\StudentController@studentcreate');
Route::post('/updateStudent/{id}', 'students\StudentController@updateStudent');
Route::delete('/deletestudent/{id}', 'students\StudentController@destroy');
Route::get('/editStudent/{id}', 'students\StudentController@editStudent');
Route::patch('update/{id}', 'students\StudentController@update');

//partner
Route::post('/getAllPartnerCampus','partner\PartnerController@getAllPartnerCampus');
Route::get('/getAllPartner','partner\PartnerController@getAllPartner');
//status
Route::get('/getAllStatus','students\StudentController@getAllStatus');

// Course controller routes
Route::get("getc",'CourseController@index');
Route::get("searchcode",'CourseController@searchcode');
Route::get("searchtitle",'CourseController@searchtitle');
Route::get("getAllCountries",'students\StudentController@getAllCountries');
Route::post('/createcourse','CourseController@createcourse');
Route::post('/updateCourse/{id}','CourseController@updateCourse');
Route::get('/getCourseByID/{id}','CourseController@getCourseByID');

// route for program select search
Route::post("/searchProgram","ProgramController@seachProgram");
Route::post("/searchAcademic","ProgramController@searchAcademic");
Route::get("getProgramName/{id}","ProgramController@getProgramName");

//Company routes
Route::post("/createCompany","ProgramController@createCompany");
Route::get("/getcompanybyID/{id}","ProgramController@getcompanybyID");
Route::post("/updateCompany/{id}","ProgramController@updateCompany");
Route::get("/deletecompany/{id}","ProgramController@deletecompany");


/// filter search routes

Route::get("getFillterList",'partner\PartnerController@getFillterList');
Route::post("searchByFilterList","students\StudentController@searchByFilterList");
// searchByFilterList
// Route::get("getStatusList",'partner\PartnerController@getStatusList');
// Route::get("getCountryList",'partner\PartnerController@getCountryList');
// Route::get("getPartnerCodeList",'partner\PartnerController@getPartnerCodeList');

///Checker route

// Route::get("delFile",function(){

//    // File::delete(public_path("test/1.jpg"));
//   return  explode('/','5c0eab7a02938/ascension-diego-fernandez.jpg')[1];

//     return public_path()."\student_attached_files"."\bffa265483f3SourceTree.exe";

// });

//mbacourse
Route::get("/getmbacourse","ProgramController@getmbacourse");

//search books
Route::get("/searchBookByTitle","BookController@searchBookByTitle");
Route::get("/searchBundleDescription","BookController@searchBundleDescription");
Route::get("/getBookByID/{id}","BookController@getBookByID");
Route::post('/updateBook/{id}','BookController@updateBook');
route::delete('/deleteBook/{id}','BookController@deleteBook');
//program search by title
Route::get("/searchProgramByTitle","ProgramController@searchProgramByTitle");
//search company by company name
Route::get("/searchByCompanyName","ProgramController@searchByCompanyName");
//staff routes
Route::post('/StaffCreate',"EmployeController@StaffCreate");
Route::get("/stafflist","EmployeController@stafflist");
Route::delete("/deletestaff/{id}","EmployeController@deletestaff");
Route::get('/searchstaff',"EmployeController@searchstaff");
Route::get('/getStaffByID/{id}',"EmployeController@getStaffByID");
Route::post('/updateStaff/{id}',"EmployeController@updateStaff");


///porgaam routes

Route::post("/saveProgram","ProgramController@saveProgram");


/// LOGOUT

// Route::post('logout', ['as' => 'logout', 'uses' => 'Auth\LoginController@logout']);


Route::get("trans/{id}",function(){
    return view('students.trans_std');
});


// Authentication Routes...

  Route::post('login', [
    'as' => '',
    'uses' => 'Auth\LoginController@login'
  ]);
  Route::post('logout', [
    'as' => 'logout',
    'uses' => 'Auth\LoginController@logout'
  ]);
// auth routes

Route::post('password/email', [
    'as' => 'password.email',
    'uses' => 'Auth\ForgotPasswordController@sendResetLinkEmail'
  ]);
  Route::get('password/reset', [
    'as' => 'password.request',
    'uses' => 'Auth\ForgotPasswordController@showLinkRequestForm'
  ]);
  Route::post('password/reset', [
    'as' => '',
    'uses' => 'Auth\ResetPasswordController@reset'
  ]);
  Route::get('password/reset/{token}', [
    'as' => 'password.reset',
    'uses' => 'Auth\ResetPasswordController@showResetForm'
  ]);
  
  // Registration Routes...

  Route::post('register','AdminController@registerUser');

  Route::get('checkLoggedIn',function ()
  {
    if(Auth::check())
    return response()->json(true);
    else
    return response()->json(false);
  });


  /// user routes

  Route::get('getUserList',"UserController@getUserList");
  Route::get('getRoleList',"UserController@getRoleList");
  Route::get('getRole/{id}',"UserController@getRole");

  

