@extends('students.index')
@section('content')


<!-- add new register parent -->
<div class="register-box">
  <div class="register-logo">
    <a href="../../index2.html">
    <img src="{{asset('logo/sbs_transp.png')}}" alt="">
    </a>
  </div>

  <div class="card">
    <div class="card-body register-card-body">
      <p class="login-box-msg">Register a new membership</p>

      <form action="{{route('register')}}" method="post">
      @csrf
        <div class="form-group has-feedback">
          <input type="text" class="form-control" placeholder="Full name"  value="{{ old('name') }}" required autofocus name="name">
          <span class="fa fa-user form-control-feedback" style="display:none"></span>
          @if ($errors->has('name'))
                <span  role="alert">
                    <strong style="color:red">{{ $errors->first('name') }}</strong>
                </span>
            @endif
        </div>
        <div class="form-group has-feedback">
          <input type="email" class="form-control"  value="{{ old('email') }}" required placeholder="Email" name="email">
          <span class="fa fa-envelope form-control-feedback"   style="display:none"></span>
          @if ($errors->has('email'))
                <span  role="alert">
                    <strong style="color:red">{{ $errors->first('email') }}</strong>
                </span>
            @endif
        </div>
        <div class="form-group has-feedback">
          <input type="password" class="form-control"  value="{{ old('password') }}" required placeholder="Password" name="password">
          <span class="fa fa-lock form-control-feedback"  style="display:none"></span>
          @if ($errors->has('password'))
                <span  role="alert">
                    <strong style="color:red">{{ $errors->first('password') }}</strong>
                </span>
            @endif
        </div>
        <div class="form-group has-feedback">
          <input type="password" class="form-control" name="password_confirmation" placeholder="Retype password" >
          <span class="fa fa-lock form-control-feedback"  style="display:none"></span>
        </div>
        <div class="row">
          <div class="col-8">
            <div class="checkbox icheck">
              <label>
                <input type="checkbox" > I agree to the <a href="#">terms</a>
              </label>
            </div>
          </div>
          <!-- /.col -->
          <div class="col-4">
            <button type="submit" class="btn btn-primary btn-block btn-flat">Register</button>
          </div>
          <!-- /.col -->
        </div>
      </form>

      <div class="social-auth-links text-center" style="display:none">
        <p>- OR -</p>
        <a href="#" class="btn btn-block btn-primary">
          <i class="fa fa-facebook mr-2"></i>
          Sign up using Facebook
        </a>
        <a href="#" class="btn btn-block btn-danger">
          <i class="fa fa-google-plus mr-2"></i>
          Sign up using Google+
        </a>
      </div>

      <a href="{{route('login')}}" class="text-center">I already have a membership</a>
    </div>
    <!-- /.form-box -->
  </div><!-- /.card -->
</div>
@endsection
