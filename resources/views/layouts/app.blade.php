<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Scripts -->


    <script src="{{ asset('js/jquery.min.js') }}"></script>
    <script src="{{ asset('js/bootstrap.min.js') }}"></script>
    <script src="{{ asset('js/app.js') }}" defer></script>
    <link href="https://netdna.bootstrapcdn.com/bootstrap/3.1.1/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <!-- <link href="http://protostrap.com/Assets/gv/css/gv.bootstrap-form.css" rel="stylesheet" type="text/css" /> -->
    <!-- Styles -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bulma/0.6.2/css/bulma.min.css">
    <link rel="stylesheet" href="{{asset('css/sidebar.css')}}">
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link href="{{ asset('css/font-awesome.min.css') }}" rel="stylesheet">
    <style>
        .main_content{
            margin-left:-10%;
            width:90%;
        }
        .date_pic{
       
        }
        </style>
</head>
<body>
    <div>
        @yield('content')
    </div>
    <!-- Bootstrap JS -->
    
    <script type="text/javascript">
        $(document).ready(function () {            
            $("#personal_info").click();
            $("#profesor_info_edit").click();
            $("#profesor_info").click();            
            $("#book_basic_info").click();
            
            $("#personal_info_btn").click(function(){
                setTimeout(function(){  $("#personal_info").click(); }, 500);     
            });  

            $("#createbook").click(function(){
                setTimeout(function(){ $("#book_basic_info").click(); }, 200);     
            });
             
            $("#profesor_info_btn").click(function(){
                setTimeout(function(){  $("#profesor_info").click();
                }, 700);
            });      
            $('#sidebarCollapse').on('click', function () {
                $('#sidebar').toggleClass('active');
                $("#content_part").toggleClass('main_content');
            });


            $(".professorEdit").click(function(){
                alert();
                setTimeout(function(){  $("#profesor_info_edit").click();
                }, 700);
            });  

            $(".date_picker").children().children().css({
                "background": "white",
                "margin-left": "-2%",
                "margin-right": "-2%",
                "cursor": "pointer",
                "border-radius": "0.25rem"});
                
            $(".date_picker_sm").children().children().css({
                "background": "white",
                "margin-left": "-7%",
                "margin-right": "-7%",
                "cursor": "pointer",
                "border-radius": "0.25rem"});
            // $(".date_picker").children().css('width','100%');
        });
    </script>
</body>

</html>
