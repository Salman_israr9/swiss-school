<!DOCTYPE html>
<html lang="en">
    <head>
 
    <meta name="csrf-token" content="{{ csrf_token() }}">
    </head>
<body>
<div id="app">
 <showtranscript></showtranscript>
</div>
<script  src="{{asset('js/app.js')}}"></script>
</body>
</html>