
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */
require('./bootstrap');

window.Vue = require('vue');
import VueRouter from 'vue-router';

import studentcreate from './components/students/CreateComponent';
import login from './components/auth/loginComponent';
import register from './components/auth/registerComponent';
import studentedit from './components/students/EditComponent';
import studentshow from './components/students/ShowComponent';
import studentindex from './components/students/index'
import studentmain from './components/students/MainComponent'
import professor from './components/staff/ProfessorCompnent'
import company from './components/Company/CompanyComponent'
import staff from './components/staff/StaffComponent'
import professorcreate from './components/staff/create'
import professorupdate from './components/staff/updateProfessor'
import addstaff from './components/staff/addStaff'
import addcompany from './components/Company/create'
import editcompany from './components/Company/editCompany'
import transcript from './components/students/Transcript/index'
import student_contact from './components/students/contact/contactList';
import book_list from './components/book/Book';
import book_create from './components/book/BookCreate';
import book_edit from './components/book/BookUpdate';
import book_boundle_list from './components/book/BookBoundle';
import book_bundle_create from './components/book/BookBundleCreate';
import book_bundle_edit from './components/book/BookBundleEdit';
import student_course from './components/course/Course';
import program_list from'./components/program/program'
import invoice_all from './components/invoice/InvoiceAll';
import student_invoice_list from './components/invoice/studentInvoice';
import student_paid_invoice from './components/invoice/StudentPaidInvoice';
import student_unpaid_invoice from './components/invoice/StudentUnpaidInvoice';
import company_paid_invoice from './components/invoice/CompanyPaidInvoice';
import company_unpaid_invoice from './components/invoice/CompanyUnpaidInvoice';
import createcourse from './components/course/create';
import createprogram from './components/program/create';
import booksubmission from './components/book/BookSubmission'
import book_received from './components/book/BookReceived'
import company_invoice from './components/invoice/CompanyInvoice'
import course_edit from './components/course/editCourse'
import student_invoice_create from './components/invoice/createStudentInvoice'
import company_invoice_create from './components/invoice/createCompanyInvoice'
import company_invoice_edit from './components/invoice/editCompanyInvoice'
import student_invoice_edit from './components/invoice/editStudentInvoice'
import showtranscript from './components/students/Transcript/TrancriptView.vue'
import showinvoice from './components/invoice/InvoiceView.vue'
import editstaff from './components/staff/updateStaff.vue'
import studentcontacts from './components/students/contact/StudentContacts.vue'
import editprogram from './components/program/updateprogram.vue'
/// users and role import
import users from './components/user/userListComponent.vue'
import editUser from './components/user/editUserComponent.vue'
import roles from './components/role/roleListComponent.vue'
import editRole from './components/role/editRoleComponent.vue'



Vue.component('register', require('./components/auth/registerComponent'));
Vue.component('login', require('./components/auth/loginComponent'));
Vue.component('studentcreate', require('./components/students/CreateComponent.vue'));
Vue.component('studentshow', require('./components/students/ShowComponent.vue'));
Vue.component('studentedit', require('./components/students/EditComponent.vue'));
Vue.component('Pagination', require('./components/pagination/PaginationComponent.vue'));
Vue.component('studentindex',require('./components/students/index.vue'));
Vue.component('studentmain',require('./components/students/MainComponent.vue'));
Vue.component('professor', require('./components/staff/ProfessorCompnent.vue'))
Vue.component('company', require('./components/Company/CompanyComponent.vue'))
Vue.component('staff', require('./components/staff/StaffComponent.vue'))
Vue.component('professorcreate', require('./components/staff/create.vue'))
Vue.component('professorupdate', require('./components/staff/updateProfessor.vue'))
Vue.component('addstaff', require('./components/staff/addStaff.vue'))
Vue.component('addcompany', require('./components/Company/create.vue'))
Vue.component('editcompany', require('./components/Company/editCompany.vue'))
Vue.component('transcript', require('./components/students/Transcript/index.vue'))
Vue.component('student_contact', require('./components/students/contact/contactList.vue'))
Vue.component('book_list', require('./components/book/Book.vue'))
Vue.component('book_create', require('./components/book/BookCreate.vue'))
Vue.component('book_edit', require('./components/book/BookUpdate.vue'))
Vue.component('book_boundle_list', require('./components/book/BookBoundle.vue'))
Vue.component('book_bundle_create', require('./components/book/BookBundleCreate.vue'))
Vue.component('book_bundle_edit', require('./components/book/BookBundleEdit.vue'))
Vue.component('student_course', require('./components/course/Course.vue'))
Vue.component('invoice_all', require('./components/invoice/InvoiceAll.vue'))
Vue.component('std_invoice_list', require('./components/invoice/studentInvoice.vue'))
Vue.component('student_paid_invoice', require('./components/invoice/StudentPaidInvoice.vue'))
Vue.component('student_unpaid_invoice', require('./components/invoice/StudentUnpaidInvoice.vue'))
Vue.component('company_paid_invoice', require('./components/invoice/CompanyPaidInvoice.vue'))
Vue.component('company_unpaid_invoice', require('./components/invoice/CompanyUnpaidInvoice.vue'))
Vue.component('program_list', require('./components/program/program.vue'))
Vue.component('createcourse',require('./components/course/create.vue'))
Vue.component('createprogram',require('./components/program/create.vue'))
Vue.component('booksubmission',require('./components/book/BookSubmission.vue'))
Vue.component('book_received',require('./components/book/BookReceived.vue'))
Vue.component('company_invoice',require('./components/invoice/CompanyInvoice.vue'))
Vue.component('course_edit',require('./components/course/editCourse.vue'))
Vue.component('student_invoice_create',require('./components/invoice/createStudentInvoice.vue'))
Vue.component('company_invoice_create',require('./components/invoice/createCompanyInvoice.vue'))
Vue.component('student_invoice_edit',require('./components/invoice/editStudentInvoice.vue'))
Vue.component('company_invoice_edit',require('./components/invoice/editCompanyInvoice.vue'))
Vue.component('showtranscript',require('./components/students/Transcript/TrancriptView.vue'))
Vue.component('showinvoice',require('./components/invoice/InvoiceView.vue'))
Vue.component('editstaff', require('./components/staff/updateStaff.vue'))
Vue.component('studentcontacts', require('./components/students/contact/StudentContacts.vue'))
Vue.component('editprogram', require('./components/program/updateprogram.vue'))

/// user and role component import 
Vue.component('users', require('./components/user/userListComponent.vue'))
Vue.component('editUser', require('./components/user/editUserComponent.vue'))
Vue.component('roles', require('./components/role/roleListComponent.vue'))
Vue.component('editRole', require('./components/role/editRoleComponent.vue'))




/**
 * The following block of code may be used to automatically register your
 * Vue components. It will recursively scan this directory for the Vue
 * components and automatically register them with their "basename".
 *
 * Eg. ./components/ExampleComponent.vue -> <example-component></example-component>
 */


// const files = require.context('./', true, /\.vue$/i)

// files.keys().map(key => {
//     return Vue.component(_.last(key.split('/')).split('.')[0], files(key))
// })

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */
Vue.use(VueRouter);
const router = new VueRouter({
    mode: 'history',
    history: true,
    routes: [
        //studentsRoutes
        {path: '/register', component: register, name: 'auth.register' },
        {path: '/student-list', component: studentindex, name: 'student.index' },
        {path: '/student/create', component: studentcreate, name: 'student.create' },
        {path: '/student/show', component: studentshow, name: 'student.show' },
        {path: '/student/edit/:Sid', component: studentedit, name: 'student.edit' },
      
        {path:'/student/main', component:studentmain, name:'student.main'},
        {path:'/professor/index', component:professor, name:'professor.index'},
        {path:'/company/index', component:company, name:'company.index'},
        {path:'/staff/index', component:staff, name:'staff.index'},
        {path:'/professor/create', component:professorcreate, name:'professor.create'},
        {path:'/professor/edit/:id', component:professorupdate, name:'professor.edit'},
        {path:'/staff/create', component:addstaff, name:'staff.create'},
        {path:'/company/create', component:addcompany, name:'company.create'},
        {path:'/company/edit/:id', component:editcompany, name:'company.edit'},
        {path:'/transcript/index', component:transcript, name:'transcript.index'},
        {path:'/student/contact/contactList', component:student_contact, name:'student.contact.contactList'},
        {path:'/book/list', component:book_list, name:'book.list'},
        {path:'/book/create', component:book_create, name:'book.create'},
        {path:'/book/edit/:id', component:book_edit, name:'book.edit'},
        {path:'/book/bundle/list', component:book_boundle_list, name:'bookbundle.list'},
        {path:'/book/bundle/create', component:book_bundle_create, name:'bookbundle.create'},
        {path:'/book/bundle/edit/:id', component:book_bundle_edit, name:'bookbundle.edit'},
        {path:'/courses/list', component:student_course, name:'students.course'},
        {path:'/all/invoice', component:invoice_all,name:'students.invoice.all'},
        {path:'/student/invoice/list', component:student_invoice_list,name:'students.invoice.list'},
        {path:'/student/paid/invoice/list', component:student_paid_invoice,name:'students.paid.invoice.list'},
        {path:'/student/unpaid/invoice/list', component:student_unpaid_invoice,name:'students.unpaid.invoice.list'},
        {path:'/company/invoice', component:company_invoice,name:'compnay.invoice'},
        {path:'/company/paid/invoice/list', component:company_paid_invoice,name:'company.paid.invoice.list'},
        {path:'/company/unpaid/invoice/list', component:company_unpaid_invoice,name:'company.unpaid.invoice.list'},
        {path:'/program/list', component:program_list,name:'program.list'},
        {path:'/course/create', component:createcourse,name:'course.create'},
        {path:'/program/create', component:createprogram,name:'program.create'},
        {path:'/book/submission', component:booksubmission,name:'book.submission'},
        {path:'/book/receive', component:book_received,name:'book.received'},
        {path:'/course/edit/:id', component:course_edit,name:'course.edit'},
        {path:'/student/invoice/create',component:student_invoice_create, name:'invoice.create'},
        {path:'/company/invoice/create',component:company_invoice_create, name:'company.invoice.create'},
        {path:'/student/invoice/edit/:id',component:student_invoice_edit, name:'student.invoice.edit'},
        {path:'/company/invoice/edit/:id',component:company_invoice_edit, name:'company.invoice.edit'},
        {path:'/transcript/show',component:showtranscript, name:'transcript.show'},
        {path:'/invoice/show',component:showinvoice, name:'invoice.show'},
        {path:'/staff/edit/:id',component:editstaff, name:'staff.edit'},
        {path:'/students/contacts/program/:id',component:studentcontacts, name:'students.contacts.program'},
        { path: '/program/edit/:id',component: editprogram, name: 'program.edit'},
        // user 
        { path: '/user/edit/:id',component: editUser},
        { path: '/user/list',component: users, name: 'users'},
        { path: '/role/list',component: roles, name: 'roles'},
        { path: '/edit/role/:id',component: editRole, name: 'edit.role'},

   
    ],

});


let show_login_page= false;

const app = new Vue({
    el: '#app',
    router
});
